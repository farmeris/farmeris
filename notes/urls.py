
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from notes import views

app_name = 'notes'

urlpatterns = [
    path('', views.notesk, name='notesk'),
    # path('en/', views.notesen, name='notesen'),
]

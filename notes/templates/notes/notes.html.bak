<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<!-- 2024-02-11 Ne 01:05 -->
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Notes</title>
<meta name="author" content="kekw" />
<meta name="generator" content="Org Mode" />
<style>
  #content { max-width: 60em; margin: auto; }
  .title  { text-align: center;
             margin-bottom: .2em; }
  .subtitle { text-align: center;
              font-size: medium;
              font-weight: bold;
              margin-top:0; }
  .todo   { font-family: monospace; color: red; }
  .done   { font-family: monospace; color: green; }
  .priority { font-family: monospace; color: orange; }
  .tag    { background-color: #eee; font-family: monospace;
            padding: 2px; font-size: 80%; font-weight: normal; }
  .timestamp { color: #bebebe; }
  .timestamp-kwd { color: #5f9ea0; }
  .org-right  { margin-left: auto; margin-right: 0px;  text-align: right; }
  .org-left   { margin-left: 0px;  margin-right: auto; text-align: left; }
  .org-center { margin-left: auto; margin-right: auto; text-align: center; }
  .underline { text-decoration: underline; }
  #postamble p, #preamble p { font-size: 90%; margin: .2em; }
  p.verse { margin-left: 3%; }
  pre {
    border: 1px solid #e6e6e6;
    border-radius: 3px;
    background-color: #f2f2f2;
    padding: 8pt;
    font-family: monospace;
    overflow: auto;
    margin: 1.2em;
  }
  pre.src {
    position: relative;
    overflow: auto;
  }
  pre.src:before {
    display: none;
    position: absolute;
    top: -8px;
    right: 12px;
    padding: 3px;
    color: #555;
    background-color: #f2f2f299;
  }
  pre.src:hover:before { display: inline; margin-top: 14px;}
  /* Languages per Org manual */
  pre.src-asymptote:before { content: 'Asymptote'; }
  pre.src-awk:before { content: 'Awk'; }
  pre.src-authinfo::before { content: 'Authinfo'; }
  pre.src-C:before { content: 'C'; }
  /* pre.src-C++ doesn't work in CSS */
  pre.src-clojure:before { content: 'Clojure'; }
  pre.src-css:before { content: 'CSS'; }
  pre.src-D:before { content: 'D'; }
  pre.src-ditaa:before { content: 'ditaa'; }
  pre.src-dot:before { content: 'Graphviz'; }
  pre.src-calc:before { content: 'Emacs Calc'; }
  pre.src-emacs-lisp:before { content: 'Emacs Lisp'; }
  pre.src-fortran:before { content: 'Fortran'; }
  pre.src-gnuplot:before { content: 'gnuplot'; }
  pre.src-haskell:before { content: 'Haskell'; }
  pre.src-hledger:before { content: 'hledger'; }
  pre.src-java:before { content: 'Java'; }
  pre.src-js:before { content: 'Javascript'; }
  pre.src-latex:before { content: 'LaTeX'; }
  pre.src-ledger:before { content: 'Ledger'; }
  pre.src-lisp:before { content: 'Lisp'; }
  pre.src-lilypond:before { content: 'Lilypond'; }
  pre.src-lua:before { content: 'Lua'; }
  pre.src-matlab:before { content: 'MATLAB'; }
  pre.src-mscgen:before { content: 'Mscgen'; }
  pre.src-ocaml:before { content: 'Objective Caml'; }
  pre.src-octave:before { content: 'Octave'; }
  pre.src-org:before { content: 'Org mode'; }
  pre.src-oz:before { content: 'OZ'; }
  pre.src-plantuml:before { content: 'Plantuml'; }
  pre.src-processing:before { content: 'Processing.js'; }
  pre.src-python:before { content: 'Python'; }
  pre.src-R:before { content: 'R'; }
  pre.src-ruby:before { content: 'Ruby'; }
  pre.src-sass:before { content: 'Sass'; }
  pre.src-scheme:before { content: 'Scheme'; }
  pre.src-screen:before { content: 'Gnu Screen'; }
  pre.src-sed:before { content: 'Sed'; }
  pre.src-sh:before { content: 'shell'; }
  pre.src-sql:before { content: 'SQL'; }
  pre.src-sqlite:before { content: 'SQLite'; }
  /* additional languages in org.el's org-babel-load-languages alist */
  pre.src-forth:before { content: 'Forth'; }
  pre.src-io:before { content: 'IO'; }
  pre.src-J:before { content: 'J'; }
  pre.src-makefile:before { content: 'Makefile'; }
  pre.src-maxima:before { content: 'Maxima'; }
  pre.src-perl:before { content: 'Perl'; }
  pre.src-picolisp:before { content: 'Pico Lisp'; }
  pre.src-scala:before { content: 'Scala'; }
  pre.src-shell:before { content: 'Shell Script'; }
  pre.src-ebnf2ps:before { content: 'ebfn2ps'; }
  /* additional language identifiers per "defun org-babel-execute"
       in ob-*.el */
  pre.src-cpp:before  { content: 'C++'; }
  pre.src-abc:before  { content: 'ABC'; }
  pre.src-coq:before  { content: 'Coq'; }
  pre.src-groovy:before  { content: 'Groovy'; }
  /* additional language identifiers from org-babel-shell-names in
     ob-shell.el: ob-shell is the only babel language using a lambda to put
     the execution function name together. */
  pre.src-bash:before  { content: 'bash'; }
  pre.src-csh:before  { content: 'csh'; }
  pre.src-ash:before  { content: 'ash'; }
  pre.src-dash:before  { content: 'dash'; }
  pre.src-ksh:before  { content: 'ksh'; }
  pre.src-mksh:before  { content: 'mksh'; }
  pre.src-posh:before  { content: 'posh'; }
  /* Additional Emacs modes also supported by the LaTeX listings package */
  pre.src-ada:before { content: 'Ada'; }
  pre.src-asm:before { content: 'Assembler'; }
  pre.src-caml:before { content: 'Caml'; }
  pre.src-delphi:before { content: 'Delphi'; }
  pre.src-html:before { content: 'HTML'; }
  pre.src-idl:before { content: 'IDL'; }
  pre.src-mercury:before { content: 'Mercury'; }
  pre.src-metapost:before { content: 'MetaPost'; }
  pre.src-modula-2:before { content: 'Modula-2'; }
  pre.src-pascal:before { content: 'Pascal'; }
  pre.src-ps:before { content: 'PostScript'; }
  pre.src-prolog:before { content: 'Prolog'; }
  pre.src-simula:before { content: 'Simula'; }
  pre.src-tcl:before { content: 'tcl'; }
  pre.src-tex:before { content: 'TeX'; }
  pre.src-plain-tex:before { content: 'Plain TeX'; }
  pre.src-verilog:before { content: 'Verilog'; }
  pre.src-vhdl:before { content: 'VHDL'; }
  pre.src-xml:before { content: 'XML'; }
  pre.src-nxml:before { content: 'XML'; }
  /* add a generic configuration mode; LaTeX export needs an additional
     (add-to-list 'org-latex-listings-langs '(conf " ")) in .emacs */
  pre.src-conf:before { content: 'Configuration File'; }

  table { border-collapse:collapse; }
  caption.t-above { caption-side: top; }
  caption.t-bottom { caption-side: bottom; }
  td, th { vertical-align:top;  }
  th.org-right  { text-align: center;  }
  th.org-left   { text-align: center;   }
  th.org-center { text-align: center; }
  td.org-right  { text-align: right;  }
  td.org-left   { text-align: left;   }
  td.org-center { text-align: center; }
  dt { font-weight: bold; }
  .footpara { display: inline; }
  .footdef  { margin-bottom: 1em; }
  .figure { padding: 1em; }
  .figure p { text-align: center; }
  .equation-container {
    display: table;
    text-align: center;
    width: 100%;
  }
  .equation {
    vertical-align: middle;
  }
  .equation-label {
    display: table-cell;
    text-align: right;
    vertical-align: middle;
  }
  .inlinetask {
    padding: 10px;
    border: 2px solid gray;
    margin: 10px;
    background: #ffffcc;
  }
  #org-div-home-and-up
   { text-align: right; font-size: 70%; white-space: nowrap; }
  textarea { overflow-x: auto; }
  .linenr { font-size: smaller }
  .code-highlighted { background-color: #ffff00; }
  .org-info-js_info-navigation { border-style: none; }
  #org-info-js_console-label
    { font-size: 10px; font-weight: bold; white-space: nowrap; }
  .org-info-js_search-highlight
    { background-color: #ffff00; color: #000000; font-weight: bold; }
  .org-svg { }
</style>
</head>
<body>
<div id="content" class="content">
<h1 class="title">Notes</h1>
<div id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#org031cfb7">1. Feed and Nutrition:</a>
<ul>
<li><a href="#orge24d36c">1.1. &ldquo;I&rsquo;d love to hear about how you manage the diet for your sheep. What kind of feed do you find works best?&rdquo;</a></li>
<li><a href="#org7a46ead">1.2. &ldquo;How do you approach ensuring the feed is safe and healthy for the sheep?&rdquo;</a>
<ul>
<li><a href="#org85bb5ff">1.2.1. GMOs: Whether the feed is genetically modified or non-GMO.</a></li>
<li><a href="#org0c7b214">1.2.2. Pesticides: Use of pesticides on the grasses and forages.</a></li>
<li><a href="#org86c115c">1.2.3. Home-Grown vs. Purchased Feed: Whether the feed is grown on the farm or purchased from outside sources.</a></li>
<li><a href="#orgdae03be">1.2.4. Supplements: Use of nutritional supplements or mineral additives.</a></li>
<li><a href="#org986813b">1.2.5. Organic Certification: Whether the feed is certified organic.</a></li>
<li><a href="#orgf7768c4">1.2.6. Feed Additives and Growth Promoters: Usage of any non-natural additives.</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="#org4fea371">2. Health Management and Welfare:</a>
<ul>
<li><a href="#org28f4b6a">2.1. &ldquo;Could you share some insights into your health management practices for the sheep?&rdquo;</a></li>
<li><a href="#org0dcf08a">2.2. &ldquo;I&rsquo;m curious about how you handle any challenges with parasites or diseases.&rdquo;</a>
<ul>
<li><a href="#org1963a8b">2.2.1. Vaccination Practices: Specific vaccines used and frequency.</a></li>
<li><a href="#org0d09362">2.2.2. Antibiotic Use: Approach to antibiotic usage and resistance management.</a></li>
<li><a href="#org45780be">2.2.3. Parasite Management: Use of chemical dewormers versus natural remedies.</a></li>
<li><a href="#org6673b39">2.2.4. Mastitis Prevention and Treatment: Specific treatments for common ailments.</a></li>
<li><a href="#org94bb016">2.2.5. Chemical Exposure: Use of chemicals in treating external parasites.</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="#orgd41e457">3. Environmental and Water Management:</a>
<ul>
<li><a href="#orgb05ee8d">3.1. &ldquo;What are some of the practices you&rsquo;ve implemented to maintain a clean and safe environment for your sheep?&rdquo;</a></li>
<li><a href="#orgc2bd954">3.2. &ldquo;How do you manage the quality of water and soil on your farm?&rdquo;</a>
<ul>
<li><a href="#org492a994">3.2.1. Contaminant Management: Efforts to prevent chemical runoff or heavy metal presence in grazing fields.</a></li>
<li><a href="#orga4d072d">3.2.2. Air and Water Quality Monitoring: How these are assessed and maintained.</a></li>
<li><a href="#orgb6ebc0b">3.2.3. Water Source and Quality: The origin of the water and any treatments applied.</a></li>
<li><a href="#org1ef9224">3.2.4. Soil Health Practices: Measures taken to ensure soil vitality and prevent erosion.</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="#org4827dbe">4. Milking Practices and Equipment:</a>
<ul>
<li><a href="#org8e14a07">4.1. &ldquo;I&rsquo;m interested in learning about your milking process. Could you describe how you ensure it&rsquo;s efficient and safe?&rdquo;</a></li>
<li><a href="#org5050d75">4.2. &ldquo;What considerations do you have when choosing equipment and materials for milking and storage?&rdquo;</a>
<ul>
<li><a href="#orge3b830d">4.2.1. Cleaning Agents: Types of cleaning agents used for equipment and their environmental impact.</a></li>
<li><a href="#orgce5db98">4.2.2. Equipment Material: Specific materials used for milking machines and containers, focusing on potential chemical leaching.</a></li>
<li><a href="#org0839960">4.2.3. Cleaning Agent Residue: Practices to ensure no residue remains on equipment.</a></li>
<li><a href="#orga94f284">4.2.4. Bacterial Growth Prevention: How equipment design minimizes the risk of contamination.</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="#org39fad59">5. Milk Storage, Handling, and Processing:</a>
<ul>
<li><a href="#org5ea25c5">5.1. &ldquo;How do you handle and store milk to maintain its quality?&rdquo;</a></li>
<li><a href="#org03a078b">5.2. &ldquo;Are there specific steps you take when preparing milk for different products?&rdquo;</a>
<ul>
<li><a href="#orgb64851c">5.2.1. Cooling and Storage Procedures: Speed of cooling and types of cooling systems used.</a></li>
<li><a href="#org6ae0544">5.2.2. Storage Tank Materials: Materials used for milk storage and potential interactions with milk.</a></li>
<li><a href="#org0818f10">5.2.3. Cross-Contamination Prevention: Steps taken during milk transfer to prevent contamination.</a></li>
<li><a href="#org3eccdb6">5.2.4. Transportation Conditions: How milk is kept at safe temperatures during transit.</a></li>
<li><a href="#orgb958a7a">5.2.5. Regulatory Compliance for Raw Milk: Measures to ensure safety without pasteurization.</a></li>
<li><a href="#org21d6e6b">5.2.6. Protocols for Different Products: Specific handling procedures for milk destined for cheese, yogurt, etc.</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="#orgc28535f">6. Stress and Animal Welfare:</a>
<ul>
<li><a href="#orgf204f30">6.1. &ldquo;What practices do you have in place to ensure the well-being and comfort of your sheep?&rdquo;</a></li>
<li><a href="#orgab8dd59">6.2. &ldquo;How do you maintain a healthy and stress-free environment for the flock?&rdquo;</a>
<ul>
<li><a href="#org321a602">6.2.1. Handling Techniques: How animals are treated during daily handling and milking to minimize stress.</a></li>
<li><a href="#orgdb83827">6.2.2. Genetic Diversity: Efforts to maintain or improve genetic diversity within the flock.</a></li>
<li><a href="#orgaea48bf">6.2.3. Living Conditions: The overall living conditions of the sheep, including space, shelter, and social structure.</a></li>
<li><a href="#org441b97b">6.2.4. Environmental Enrichment: Whether and how the farm provides stimulation and comfort to the animals.</a></li>
</ul>
</li>
</ul>
</li>
</ul>
</div>
</div>
<div id="outline-container-org031cfb7" class="outline-2">
<h2 id="org031cfb7"><span class="section-number-2">1.</span> Feed and Nutrition:</h2>
<div class="outline-text-2" id="text-1">
</div>
<div id="outline-container-orge24d36c" class="outline-3">
<h3 id="orge24d36c"><span class="section-number-3">1.1.</span> &ldquo;I&rsquo;d love to hear about how you manage the diet for your sheep. What kind of feed do you find works best?&rdquo;</h3>
</div>
<div id="outline-container-org7a46ead" class="outline-3">
<h3 id="org7a46ead"><span class="section-number-3">1.2.</span> &ldquo;How do you approach ensuring the feed is safe and healthy for the sheep?&rdquo;</h3>
<div class="outline-text-3" id="text-1-2">
</div>
<div id="outline-container-org85bb5ff" class="outline-4">
<h4 id="org85bb5ff"><span class="section-number-4">1.2.1.</span> GMOs: Whether the feed is genetically modified or non-GMO.</h4>
</div>
<div id="outline-container-org0c7b214" class="outline-4">
<h4 id="org0c7b214"><span class="section-number-4">1.2.2.</span> Pesticides: Use of pesticides on the grasses and forages.</h4>
</div>
<div id="outline-container-org86c115c" class="outline-4">
<h4 id="org86c115c"><span class="section-number-4">1.2.3.</span> Home-Grown vs. Purchased Feed: Whether the feed is grown on the farm or purchased from outside sources.</h4>
</div>
<div id="outline-container-orgdae03be" class="outline-4">
<h4 id="orgdae03be"><span class="section-number-4">1.2.4.</span> Supplements: Use of nutritional supplements or mineral additives.</h4>
</div>
<div id="outline-container-org986813b" class="outline-4">
<h4 id="org986813b"><span class="section-number-4">1.2.5.</span> Organic Certification: Whether the feed is certified organic.</h4>
</div>
<div id="outline-container-orgf7768c4" class="outline-4">
<h4 id="orgf7768c4"><span class="section-number-4">1.2.6.</span> Feed Additives and Growth Promoters: Usage of any non-natural additives.</h4>
</div>
</div>
</div>
<div id="outline-container-org4fea371" class="outline-2">
<h2 id="org4fea371"><span class="section-number-2">2.</span> Health Management and Welfare:</h2>
<div class="outline-text-2" id="text-2">
</div>
<div id="outline-container-org28f4b6a" class="outline-3">
<h3 id="org28f4b6a"><span class="section-number-3">2.1.</span> &ldquo;Could you share some insights into your health management practices for the sheep?&rdquo;</h3>
</div>
<div id="outline-container-org0dcf08a" class="outline-3">
<h3 id="org0dcf08a"><span class="section-number-3">2.2.</span> &ldquo;I&rsquo;m curious about how you handle any challenges with parasites or diseases.&rdquo;</h3>
<div class="outline-text-3" id="text-2-2">
</div>
<div id="outline-container-org1963a8b" class="outline-4">
<h4 id="org1963a8b"><span class="section-number-4">2.2.1.</span> Vaccination Practices: Specific vaccines used and frequency.</h4>
</div>
<div id="outline-container-org0d09362" class="outline-4">
<h4 id="org0d09362"><span class="section-number-4">2.2.2.</span> Antibiotic Use: Approach to antibiotic usage and resistance management.</h4>
</div>
<div id="outline-container-org45780be" class="outline-4">
<h4 id="org45780be"><span class="section-number-4">2.2.3.</span> Parasite Management: Use of chemical dewormers versus natural remedies.</h4>
</div>
<div id="outline-container-org6673b39" class="outline-4">
<h4 id="org6673b39"><span class="section-number-4">2.2.4.</span> Mastitis Prevention and Treatment: Specific treatments for common ailments.</h4>
</div>
<div id="outline-container-org94bb016" class="outline-4">
<h4 id="org94bb016"><span class="section-number-4">2.2.5.</span> Chemical Exposure: Use of chemicals in treating external parasites.</h4>
</div>
</div>
</div>
<div id="outline-container-orgd41e457" class="outline-2">
<h2 id="orgd41e457"><span class="section-number-2">3.</span> Environmental and Water Management:</h2>
<div class="outline-text-2" id="text-3">
</div>
<div id="outline-container-orgb05ee8d" class="outline-3">
<h3 id="orgb05ee8d"><span class="section-number-3">3.1.</span> &ldquo;What are some of the practices you&rsquo;ve implemented to maintain a clean and safe environment for your sheep?&rdquo;</h3>
</div>
<div id="outline-container-orgc2bd954" class="outline-3">
<h3 id="orgc2bd954"><span class="section-number-3">3.2.</span> &ldquo;How do you manage the quality of water and soil on your farm?&rdquo;</h3>
<div class="outline-text-3" id="text-3-2">
</div>
<div id="outline-container-org492a994" class="outline-4">
<h4 id="org492a994"><span class="section-number-4">3.2.1.</span> Contaminant Management: Efforts to prevent chemical runoff or heavy metal presence in grazing fields.</h4>
</div>
<div id="outline-container-orga4d072d" class="outline-4">
<h4 id="orga4d072d"><span class="section-number-4">3.2.2.</span> Air and Water Quality Monitoring: How these are assessed and maintained.</h4>
</div>
<div id="outline-container-orgb6ebc0b" class="outline-4">
<h4 id="orgb6ebc0b"><span class="section-number-4">3.2.3.</span> Water Source and Quality: The origin of the water and any treatments applied.</h4>
</div>
<div id="outline-container-org1ef9224" class="outline-4">
<h4 id="org1ef9224"><span class="section-number-4">3.2.4.</span> Soil Health Practices: Measures taken to ensure soil vitality and prevent erosion.</h4>
</div>
</div>
</div>
<div id="outline-container-org4827dbe" class="outline-2">
<h2 id="org4827dbe"><span class="section-number-2">4.</span> Milking Practices and Equipment:</h2>
<div class="outline-text-2" id="text-4">
</div>
<div id="outline-container-org8e14a07" class="outline-3">
<h3 id="org8e14a07"><span class="section-number-3">4.1.</span> &ldquo;I&rsquo;m interested in learning about your milking process. Could you describe how you ensure it&rsquo;s efficient and safe?&rdquo;</h3>
</div>
<div id="outline-container-org5050d75" class="outline-3">
<h3 id="org5050d75"><span class="section-number-3">4.2.</span> &ldquo;What considerations do you have when choosing equipment and materials for milking and storage?&rdquo;</h3>
<div class="outline-text-3" id="text-4-2">
</div>
<div id="outline-container-orge3b830d" class="outline-4">
<h4 id="orge3b830d"><span class="section-number-4">4.2.1.</span> Cleaning Agents: Types of cleaning agents used for equipment and their environmental impact.</h4>
</div>
<div id="outline-container-orgce5db98" class="outline-4">
<h4 id="orgce5db98"><span class="section-number-4">4.2.2.</span> Equipment Material: Specific materials used for milking machines and containers, focusing on potential chemical leaching.</h4>
</div>
<div id="outline-container-org0839960" class="outline-4">
<h4 id="org0839960"><span class="section-number-4">4.2.3.</span> Cleaning Agent Residue: Practices to ensure no residue remains on equipment.</h4>
</div>
<div id="outline-container-orga94f284" class="outline-4">
<h4 id="orga94f284"><span class="section-number-4">4.2.4.</span> Bacterial Growth Prevention: How equipment design minimizes the risk of contamination.</h4>
</div>
</div>
</div>
<div id="outline-container-org39fad59" class="outline-2">
<h2 id="org39fad59"><span class="section-number-2">5.</span> Milk Storage, Handling, and Processing:</h2>
<div class="outline-text-2" id="text-5">
</div>
<div id="outline-container-org5ea25c5" class="outline-3">
<h3 id="org5ea25c5"><span class="section-number-3">5.1.</span> &ldquo;How do you handle and store milk to maintain its quality?&rdquo;</h3>
</div>
<div id="outline-container-org03a078b" class="outline-3">
<h3 id="org03a078b"><span class="section-number-3">5.2.</span> &ldquo;Are there specific steps you take when preparing milk for different products?&rdquo;</h3>
<div class="outline-text-3" id="text-5-2">
</div>
<div id="outline-container-orgb64851c" class="outline-4">
<h4 id="orgb64851c"><span class="section-number-4">5.2.1.</span> Cooling and Storage Procedures: Speed of cooling and types of cooling systems used.</h4>
</div>
<div id="outline-container-org6ae0544" class="outline-4">
<h4 id="org6ae0544"><span class="section-number-4">5.2.2.</span> Storage Tank Materials: Materials used for milk storage and potential interactions with milk.</h4>
</div>
<div id="outline-container-org0818f10" class="outline-4">
<h4 id="org0818f10"><span class="section-number-4">5.2.3.</span> Cross-Contamination Prevention: Steps taken during milk transfer to prevent contamination.</h4>
</div>
<div id="outline-container-org3eccdb6" class="outline-4">
<h4 id="org3eccdb6"><span class="section-number-4">5.2.4.</span> Transportation Conditions: How milk is kept at safe temperatures during transit.</h4>
</div>
<div id="outline-container-orgb958a7a" class="outline-4">
<h4 id="orgb958a7a"><span class="section-number-4">5.2.5.</span> Regulatory Compliance for Raw Milk: Measures to ensure safety without pasteurization.</h4>
</div>
<div id="outline-container-org21d6e6b" class="outline-4">
<h4 id="org21d6e6b"><span class="section-number-4">5.2.6.</span> Protocols for Different Products: Specific handling procedures for milk destined for cheese, yogurt, etc.</h4>
</div>
</div>
</div>
<div id="outline-container-orgc28535f" class="outline-2">
<h2 id="orgc28535f"><span class="section-number-2">6.</span> Stress and Animal Welfare:</h2>
<div class="outline-text-2" id="text-6">
</div>
<div id="outline-container-orgf204f30" class="outline-3">
<h3 id="orgf204f30"><span class="section-number-3">6.1.</span> &ldquo;What practices do you have in place to ensure the well-being and comfort of your sheep?&rdquo;</h3>
</div>
<div id="outline-container-orgab8dd59" class="outline-3">
<h3 id="orgab8dd59"><span class="section-number-3">6.2.</span> &ldquo;How do you maintain a healthy and stress-free environment for the flock?&rdquo;</h3>
<div class="outline-text-3" id="text-6-2">
</div>
<div id="outline-container-org321a602" class="outline-4">
<h4 id="org321a602"><span class="section-number-4">6.2.1.</span> Handling Techniques: How animals are treated during daily handling and milking to minimize stress.</h4>
</div>
<div id="outline-container-orgdb83827" class="outline-4">
<h4 id="orgdb83827"><span class="section-number-4">6.2.2.</span> Genetic Diversity: Efforts to maintain or improve genetic diversity within the flock.</h4>
</div>
<div id="outline-container-orgaea48bf" class="outline-4">
<h4 id="orgaea48bf"><span class="section-number-4">6.2.3.</span> Living Conditions: The overall living conditions of the sheep, including space, shelter, and social structure.</h4>
</div>
<div id="outline-container-org441b97b" class="outline-4">
<h4 id="org441b97b"><span class="section-number-4">6.2.4.</span> Environmental Enrichment: Whether and how the farm provides stimulation and comfort to the animals.</h4>
</div>
</div>
</div>
</div>
<div id="postamble" class="status">
<p class="author">Author: kekw</p>
<p class="date">Created: 2024-02-11 Ne 01:05</p>
</div>
</body>
</html>
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from search import views as search_views

app_name = 'search'

urlpatterns = [
    path('search/', search_views.search, name='user_search'),
    path('search/users/', search_views.search2, name='user_search2'),
]

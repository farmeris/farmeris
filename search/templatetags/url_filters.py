
from urllib.parse import urlparse

from django import template

register = template.Library()

@register.filter(name='clean_url')
def clean_url(url):
    if not url:
        return url  # If the URL is None or empty, just return it
    parsed_url = urlparse(url)
    clean_netloc = parsed_url.netloc.replace('www.', '')
    # Append the path component if you want to keep the path in the displayed URL.
    clean_path = parsed_url.path if parsed_url.path != '/' else ''
    return clean_netloc + clean_path

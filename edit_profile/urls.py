
#edit_profile/urls.py

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from edit_profile import views as edit_profile_views

app_name = 'edit_profile'

urlpatterns = [
    path('<str:username>/settings/edit', edit_profile_views.profile_editing, name='user_profile_editing'),
    path('accounts/become-seller', edit_profile_views.update_seller_status, name='update_seller_status'),
    path('<str:username>/offshores', edit_profile_views.add_pobocka, name='add_pobocka'),
    path('<str:username>/delete/pobocka/<int:pobocka_id>/', edit_profile_views.delete_pobocka, name='delete_pobocka'),
]

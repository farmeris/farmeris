
#edit_profile/forms.py

import re

from django import forms
from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator, URLValidator
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _
from edit_profile.models import Pobocka
from geopy.exc import GeocoderTimedOut
from geopy.geocoders import Nominatim
from login_app.constants import CURRENCY_CHOICES
from login_app.models import UserProfile
from login_app.validators import (validate_facebook_url,
                                  validate_instagram_url, validate_tiktok_url,
                                  validate_twitter_url, validate_youtube_url)


class UserProfileForm(forms.ModelForm):

    avatar = forms.ImageField(required=False)
    website = forms.URLField(required=False, validators=[URLValidator()])

    facebook = forms.URLField(required=False, validators=[URLValidator(), validate_facebook_url])
    twitter = forms.URLField(required=False, validators=[URLValidator(), validate_twitter_url])
    instagram = forms.URLField(required=False, validators=[URLValidator(), validate_instagram_url])
    tiktok = forms.URLField(required=False, validators=[URLValidator(), validate_tiktok_url])
    youtube = forms.URLField(required=False, validators=[URLValidator(), validate_youtube_url])

    phone_country_code = forms.CharField(
        max_length=6,
        required=False,
        initial='+421',
        label=_("Phone country code"),
        validators=[
            RegexValidator(
                regex=r'^(\+\s?\d{1,4}|00\s?\d{1,3})$',
                message=_("Enter a valid country code. Allowed formats: '+123', '00 123', '00123'.")
            )
        ]
    )
    phone_number = forms.CharField(max_length=20, required=False)
    languages = forms.CharField(max_length=100, required=False)
    bio = forms.CharField(widget=forms.Textarea, required=False)
    preferred_currency = forms.ChoiceField(choices=CURRENCY_CHOICES, label=_("Preferred Currency"), initial='EUR', required=False)

    # Company information fields
    company_name = forms.CharField(max_length=255, required=False)
    company_id_number = forms.CharField(max_length=10, required=False, validators=[RegexValidator(r'^\d{8,10}$', message=_("Enter a valid IČO."))])
    tax_id_number = forms.CharField(max_length=12, required=False)
    vat_number = forms.CharField(max_length=14, required=False, validators=[RegexValidator(r'^[a-zA-Z]{2}\d{8,10}$', message=_("Enter a valid VAT number."))])
    company_address = forms.CharField(max_length=255, required=False)

    class Meta:
        model = UserProfile
        fields = [
            'avatar', 'website',
            'facebook', 'twitter', 'instagram', 'tiktok', 'youtube',
            'phone_country_code', 'phone_number', 'languages', 'bio', 'preferred_currency',
            'company_name', 'company_id_number', 'tax_id_number', 'vat_number', 'company_address',
        ]

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        # Initialize your form here if needed. For example, to not enforce a particular field requirement:
        self.fields['phone_country_code'].widget.attrs.update({'placeholder': '+421'})
        for field in self.fields.values():
            field.required = False

    def clean_phone_country_code(self):
        default_code = '+421'
        code = self.cleaned_data.get("phone_country_code", "").strip()

        # Normalize the code by removing spaces and ensuring a '+' prefix
        code = code.replace(" ", "")
        if code.startswith("00"):
            code = "+" + code[2:]

        # Validate the normalized code
        if not re.match(r'^\+\d{1,4}$', code):
            # If the code doesn't match the expected format, revert to the default
            return default_code

        # Further check for the length of numbers after '+'
        number_part = code[1:]  # Exclude the '+'
        if len(number_part) > 3 or not number_part.isdigit():
            # If the number part is longer than 3 digits or not purely numeric, use the default
            return default_code

        return code

    def clean_phone_number(self):
        phone_number = self.cleaned_data.get('phone_number', '').replace(" ", "")
        # Remove all non-digit characters, spaces, and hyphens.
        cleaned_phone_number = re.sub(r'[^\d]', '', phone_number)

        # Check if the cleaned phone number starts with '0' and remove it
        if cleaned_phone_number.startswith('0'):
            cleaned_phone_number = cleaned_phone_number[1:]

        if cleaned_phone_number == "":
            return ''

        return cleaned_phone_number

    def clean(self):
        cleaned_data = super().clean()

        return cleaned_data

class PobockaForm(forms.ModelForm):

    street_address = forms.CharField(max_length=255, required=False)
    zip_postal_code = forms.CharField(max_length=6, required=False)
    country = forms.CharField(max_length=50, required=False)
    notes = forms.CharField(widget=forms.Textarea(attrs={'placeholder': _('Add notes about this address')}), required=False)
    # longitude = forms.CharField(max_length=50, required=False)
    # latitude = forms.CharField(max_length=50, required=False)

    class Meta:
        model = Pobocka
        fields = ['street_address', 'zip_postal_code', 'country', 'longitude', 'latitude', 'detailed_location', 'city', 'state_province', 'notes']

    def clean_zip_postal_code(self):
        zip_postal_code = self.cleaned_data.get('zip_postal_code', '').replace(" ", "")
        if not zip_postal_code.isdigit() or (len(zip_postal_code) < 5 or len(zip_postal_code) > 6):
            raise forms.ValidationError(_("Please enter a valid postal code."))
        return zip_postal_code

    def clean(self):
        cleaned_data = super().clean()

        geolocator = Nominatim(user_agent="priec@farmeris.sk")

        # Combine the address fields to form a full address
        full_address = f"{cleaned_data.get('street_address')}, {cleaned_data.get('zip_postal_code')}, {cleaned_data.get('country')}"

        try:
            # Perform forward geocoding to fetch coordinates
            location = geolocator.geocode(full_address)
            if not location:
                raise ValidationError(_("Could not geocode the provided address. Please check and try again."))

             # Reverse geocode to validate address components
            reverse_location = geolocator.reverse((location.latitude, location.longitude), exactly_one=True, language='sk')
            if not reverse_location or 'address' not in reverse_location.raw:
                raise ValidationError(_("Could not reverse geocode the provided coordinates."))

            if location:
                # If geocoding was successful, store the coordinates
                cleaned_data['latitude'] = location.latitude
                cleaned_data['longitude'] = location.longitude
                self.instance.latitude = location.latitude
                self.instance.longitude = location.longitude
                cleaned_data['location_pobocka'] = Point(location.longitude, location.latitude)

                # Perform reverse geocoding to fetch detailed location info
                reverse_location = geolocator.reverse((location.latitude, location.longitude), exactly_one=True, language='sk')

                if reverse_location and 'address' in reverse_location.raw:
                    address = reverse_location.raw['address']
                    cleaned_data['state_province'] = address.get('state', address.get('region', None))
                    cleaned_data['city'] = address.get('city', address.get('town', address.get('village')))
                    cleaned_data['detailed_location'] = address.get('suburb') or address.get('neighbourhood') or address.get('city_district', None)
                    street_name = address.get('road', '')
                    house_number = address.get('house_number', '')
                    full_street_address = f"{street_name} {house_number}".strip()
                    cleaned_data['street_address'] = full_street_address
                    self.instance.street_address = full_street_address
                    cleaned_data['zip_postal_code'] = address.get('postcode', '')
                    self.instance.zip_postal_code = address.get('postcode', '')
                    cleaned_data['country'] = address.get('country', '')
                    self.instance.country = address.get('country', '')


        except GeocoderTimedOut:
            # You could raise a validation error or handle the geocoding failure differently
            pass

        return cleaned_data


class PobockaCoordinatesForm(forms.ModelForm):
    longitude = forms.CharField(required=True, label=_("Longitude"), widget=forms.NumberInput(attrs={'placeholder': '46.5124'}))
    latitude = forms.CharField(required=True, label=_("Latitude"), widget=forms.NumberInput(attrs={'placeholder': '53.0845'}))
    country = forms.CharField(required=True, label=_("Country"), widget=forms.TextInput(attrs={'placeholder': _('Country')}))
    notes = forms.CharField(widget=forms.Textarea(attrs={'placeholder': _('Add notes about this address')}), required=False)

    class Meta:
        model = Pobocka
        fields = ['longitude', 'latitude', 'country', 'notes', 'location_pobocka']

    def clean_longitude(self):
        longitude_str = self.cleaned_data.get('longitude', '').replace(',', '.')
        try:
            longitude = float(longitude_str)
        except ValueError:
            raise forms.ValidationError(_("Invalid longitude value. Please enter a valid number."))
        return longitude

    def clean_latitude(self):
        latitude_str = self.cleaned_data.get('latitude', '').replace(',', '.')
        try:
            latitude = float(latitude_str)
        except ValueError:
            raise forms.ValidationError(_("Invalid latitude value. Please enter a valid number."))
        return latitude

    def clean(self):
        cleaned_data = super().clean()

        latitude = cleaned_data.get('latitude')
        longitude = cleaned_data.get('longitude')
        country = cleaned_data.get('country')
        cleaned_data['location_pobocka'] = Point(longitude, latitude)

        geolocator = Nominatim(user_agent="priec@farmeris.sk")
        try:
            # Use the country to refine the reverse geocoding search
            location = geolocator.reverse(f"{latitude}, {longitude}", exactly_one=True, language='sk', addressdetails=True)
            if not location:
                raise ValidationError(_("Could not reverse geocode the given coordinates."))

            address = location.raw.get('address', {})

            # Add street number to street address if available
            street_name = address.get('road', '')
            house_number = address.get('house_number', '')
            if not street_name:
                full_street_address = ''
            else:
                full_street_address = f"{street_name} {house_number}".strip()

            # Populate the model instance directly
            self.instance.state_province = address.get('state', None)
            self.instance.city = address.get('city', address.get('town', address.get('village', None)))
            self.instance.detailed_location = address.get('suburb', None) or address.get('neighbourhood', None) or address.get('city_district', None)
            self.instance.street_address = full_street_address
            self.instance.zip_postal_code = address.get('postcode', None)

        except GeocoderTimedOut:
            raise ValidationError(_("Geocoding service timeout. Please try again."))
        except Exception as e:
            raise ValidationError(str(e))

        # Ensure instance fields are updated
        self.instance.latitude = latitude
        self.instance.longitude = longitude

        return cleaned_data

    def save(self, commit=True):
        # Ensure the Point is saved to the model instance
        instance = super().save(commit=False)
        instance.location_pobocka = self.cleaned_data.get('location_pobocka')

        if commit:
            instance.save()
            self._save_m2m()

        return instance

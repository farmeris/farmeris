
#edit_profile/views.py

import io
import uuid
from datetime import datetime

import pytz
from add_product_main.models import Polozka
from add_product_main.utils import is_valid_image
from allauth.account.models import EmailConfirmation, EmailConfirmationHMAC
from allauth.account.utils import send_email_confirmation
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.core.files.base import ContentFile
from django.db import IntegrityError, transaction
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.text import slugify
from django.utils.timezone import now
from django.utils.translation import gettext as _
from edit_profile.forms import (PobockaCoordinatesForm, PobockaForm,
                                UserProfileForm)
from edit_profile.models import Pobocka
from login_app.models import UserProfile
from staticmap import CircleMarker, StaticMap


@login_required
def profile_editing(request, username):
    user = get_object_or_404(User, username=username)
    user_profile = UserProfile.objects.get(user__username=username)

    if request.user.username != username:
        return HttpResponseForbidden("You can't edit someone else's profile!")

    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES, instance=user_profile)

        if form.is_valid():
            avatar_file = request.FILES.get('avatar')
            if avatar_file:
                valid_content_types = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif']
                if avatar_file.content_type in valid_content_types:
                    if is_valid_image(avatar_file):
                        # If the avatar is valid, it's already assigned to the form and will be saved with form.save()
                        messages.success(request, 'Profile picture updated successfully!')
                    else:
                        form.add_error('avatar', 'Invalid image file. Please ensure the image is a valid format and size.')
                        return render(request, 'settings_edit.html', {'form': form, 'username': username, 'user_profile': user_profile})
                else:
                    form.add_error('avatar', 'Unsupported file type. Please upload JPEG, JPG, PNG, or GIF images.')
                    return render(request, 'settings_edit.html', {'form': form, 'username': username, 'user_profile': user_profile})

            address_parts = [
                form.cleaned_data.get('street_address'),
                # form.cleaned_data.get('city'),
                form.cleaned_data.get('state_province'),
                form.cleaned_data.get('zip_postal_code'),
                form.cleaned_data.get('country')
            ]
            full_address = ', '.join(filter(None, address_parts))  # Filters out any None or empty strings

            with transaction.atomic():
                # form.custom_save(request)
                user_profile_instance = form.save()
                messages.success(request, 'Profile updated successfully!')

            submit_button = request.POST.get('submit_button', '')
            if submit_button == 'profileInfo':
                fragment = '#profile-info-content'
            elif submit_button == 'addressInfo':
                fragment = '#address-content'
            elif submit_button == 'companyInfo':
                fragment = '#company-content'
            else:
                fragment = ''  # No fragment for other cases

            # Construct the redirect URL with the fragment
            redirect_url = reverse('edit_profile:user_profile_editing', kwargs={'username': username}) + fragment
            return redirect(redirect_url)
            #return redirect('edit_profile:user_profile_editing', username=username)
        else:
            # If the form is not valid, render the page again with form errors.
            messages.error(request, 'Please correct the error below.')
    else:
        form = UserProfileForm(instance=user_profile)

    context = {
        'username': username,
        'user_profile': user_profile,
        'form': form,
        'current_view_name': 'user_profile_editing',
    }
    return render(request, 'settings_edit.html', context)

@login_required
def update_seller_status(request):
    user_profile = UserProfile.objects.get(user=request.user)

    if request.method == 'POST':
        user_profile.is_seller = True
        user_profile.save()
        messages.success(request, _("Congratulations! You are now a seller."))
        return redirect('main:main')  # Adjust the redirect as needed

    # For GET requests or if the user is not already a seller
    return render(request, 'settings_edit_seller.html', {'user_profile': user_profile})

@login_required
def delete_pobocka(request, username, pobocka_id):
    # Fetch the User instance using username
    user = get_object_or_404(User, username=username)

    # Ensure the User making the request matches the username in the URL
    if request.user != user:
        return HttpResponseForbidden("You can't delete someone else's address.")

    # Fetch the Pobocka instance ensuring it belongs to the User
    pobocka = get_object_or_404(Pobocka, id=pobocka_id, user_profile__user=user)

    # Perform the delete operation
    pobocka.delete()

    messages.success(request, "Pickup address successfully deleted.")

    return redirect('edit_profile:add_pobocka', username=username)

def generate_map_image(longitude, latitude):
    m = StaticMap(450, 450, url_template='http://a.tile.osm.org/{z}/{x}/{y}.png')
    outer_marker = CircleMarker((longitude, latitude), '#FFFFFF', 13)  # white border
    inner_marker = CircleMarker((longitude, latitude), 'red', 9)  # blue center

    # Add both the border marker and the center marker to create a bordered effect
    m.add_marker(outer_marker)
    m.add_marker(inner_marker)

    image = m.render(zoom=14)
    image_bytes = io.BytesIO()
    image.save(image_bytes, format='PNG')
    image_bytes.seek(0)
    return ContentFile(image_bytes.getvalue())

@login_required
def add_pobocka(request, username):
    user = get_object_or_404(User, username=username)
    user_profile = UserProfile.objects.get(user__username=username)

    if request.user.username != username:
        return HttpResponseForbidden("You can't edit someone else's profile!")

    if request.method == 'POST':
        # Determine which form to use based on the request data
        if 'longitude' in request.POST and 'latitude' in request.POST:
            coordinates_form = PobockaCoordinatesForm(request.POST, request.FILES)
            form = PobockaForm()
            if coordinates_form.is_valid():
                with transaction.atomic():
                    pobocka = coordinates_form.save(commit=False)
                    pobocka.user_profile = user_profile

                    # Generate the map image content
                    map_image_content = generate_map_image(pobocka.longitude, pobocka.latitude)

                    # Create a unique filename for the map image
                    timestamp = now().strftime("%d-%m-%Y--%H-%M-%S")
                    username = slugify(user_profile.user.username)
                    unique_id = uuid.uuid4().hex[:8]
                    image_filename = f"pobocka_map_{username}_{timestamp}_{unique_id}.png"

                    # Save the map image with the unique filename
                    pobocka.map_image.save(image_filename, map_image_content, save=False)

                    pobocka.save()
                    messages.success(request, "Pickup address added successfully using coordinates.")
                    return redirect('edit_profile:add_pobocka', username=username)

        else:
            form = PobockaForm(request.POST, request.FILES)
            coordinates_form = PobockaCoordinatesForm()

            if form.is_valid():
                with transaction.atomic():
                    pobocka = form.save(commit=False)
                    pobocka.user_profile = user_profile

                    # Generate the map image content
                    map_image_content = generate_map_image(pobocka.longitude, pobocka.latitude)

                    # Create a unique filename for the map image
                    timestamp = now().strftime("%d-%m-%Y--%H-%M-%S")
                    username = slugify(user_profile.user.username)
                    unique_id = uuid.uuid4().hex[:8]
                    image_filename = f"pobocka_map_{username}_{timestamp}_{unique_id}.png"

                    # Save the map image with the unique filename
                    pobocka.map_image.save(image_filename, map_image_content, save=False)

                    pobocka.save()
                    messages.success(request, "Pickup address added successfully.")
                    return redirect('edit_profile:add_pobocka', username=username)
    else:
        form = PobockaForm()
        coordinates_form = PobockaCoordinatesForm()

    pobocky = Pobocka.objects.filter(user_profile=user_profile)

    context = {
        'username': username,
        'user_profile': user_profile,
        'form': form,
        'coordinates_form': coordinates_form,
        'current_view_name': 'add_pobocka',
        'pobocky': pobocky,
    }
    return render(request, 'pobocka.html', context)



// edit_profile/pobocka.js

document.addEventListener('DOMContentLoaded', function() {
    // The map is initialized with one layer that pulls tiles from OpenStreetMap (OSM), centered around a specific longitude and latitude, with a zoom level of 8.
    var map = new ol.Map({
        target: 'map',
        layers: [new ol.layer.Tile({ source: new ol.source.OSM() })],
        view: new ol.View({
            center: ol.proj.fromLonLat([19.042413364820135, 49.11711885]),
            zoom: 8
        })
    });

    // Fetch the GeoJSON data
    fetch('/api/userlocations', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(data => {
            // Logic to handle successful data fetching
        var vectorSource = new ol.source.Vector({
            features: new ol.format.GeoJSON().readFeatures(data, {
                dataProjection: 'EPSG:4326',
                featureProjection: 'EPSG:3857'
            })
        });

        var vectorLayer = new ol.layer.Vector({
            source: vectorSource,
            style: new ol.style.Style({
                image: new ol.style.Icon({
                    anchor: [0.5, 46],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    src: '/static/media/marker-icon-2x-red.png',
                    scale: 0.5
                })
            })
        });

        map.addLayer(vectorLayer);
    })
    .catch((error) => {
        console.error('Error:', error);
    });

    var infoMap= document.getElementById('info-map');
    var usernameLink = infoMap.querySelector('.info-map-username');

    // Create an overlay to anchor the popup to the map.
    var overlay = new ol.Overlay({
        element: infoMap,
        positioning: 'bottom-center',
        stopEvent: true,
        offset: [0, -20]
    });
    map.addOverlay(overlay);


});

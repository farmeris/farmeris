
#edit_profile/models.py

import os
import re
import uuid

from django.apps import apps
# from edit_profile.models import Pobocka
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.geos import Point
from django.core.validators import RegexValidator
from django.db import models, transaction
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from login_app.models import UserProfile
from django.contrib.gis.db.models import PointField
from django.contrib.gis.db.models import Collect
from django.db.models import Count


def upload_pobocka_map(instance, filename):
    if hasattr(instance, 'user_profile') and instance.user_profile is not None:
        truncated_username = slugify(instance.user_profile.user.username[:10])
    else:
        truncated_username = "default"

    upload_path = os.path.join('obrasteky', truncated_username, 'pobocka_maps', filename)

    return upload_path

class Pobocka(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='pobocky')

    street_address = models.CharField(_('street address'), max_length=255, blank=True, null=True)
    city = models.CharField(_('city'), max_length=50, blank=True, null=True)
    state_province = models.CharField(_('state/province'), max_length=50, blank=True, null=True)
    detailed_location = models.CharField(_('detailed location'), max_length=255, blank=True, null=True)
    zip_postal_code = models.CharField(_('ZIP/postal code'), max_length=6, blank=True, null=True, validators=[
        RegexValidator(
            r'^\d+(\s\d+)*$',
            message=_("Enter a valid postal code. Only numbers and spaces are allowed.")
        )]
    )
    country = models.CharField(_('country'), max_length=50, blank=True, null=True, default=_('Slovakia'))
    notes = models.TextField(_('notes'), blank=True, null=True, help_text=_('Add any notes about this address'))

    latitude = models.FloatField(_('latitude'), null=True, blank=True)
    longitude = models.FloatField(_('longitude'), null=True, blank=True)
    location_pobocka = gis_models.PointField(geography=True, null=True, blank=True)

    map_image = models.ImageField(upload_to=upload_pobocka_map, null=True, blank=True)

    pobocka_name = models.CharField(max_length=255, editable=False, unique=True, blank=True)

    class Meta:
        verbose_name = _("branch")
        verbose_name_plural = _("branches")

    def generate_pobocka_name(self):
        truncated_username = slugify(self.user_profile.user.username[:10])
        address_without_numbers = re.sub(r'\d+', '', self.street_address).strip() if self.street_address else ""

        # Check if we have a valid address_without_numbers, otherwise use state_province or "default"
        if not address_without_numbers:
            base_name = slugify(self.state_province) if self.state_province else "default"
        else:
            base_name = slugify(address_without_numbers)
        unique_id = uuid.uuid4().hex[:8]  # Using a shortened version for simplicity
        return f"{truncated_username}_{base_name}_{unique_id}"

    def save(self, *args, **kwargs):
        if not self.pobocka_name:
            self.pobocka_name = self.generate_pobocka_name()
        if self.longitude and self.latitude:
            self.location_pobocka = Point(self.longitude, self.latitude)

        super().save(*args, **kwargs)

        Polozka = apps.get_model('add_product_main', 'Polozka')
        PolozkaPobocka = apps.get_model('add_product_main', 'PolozkaPobocka')

        # Now, find all Polozka instances where associate_all_pobocky is True
        polozky_to_associate = Polozka.objects.filter(
            associate_all_pobocky=True,
            user_profile=self.user_profile
        )

        if polozky_to_associate.exists():  # Check if the queryset is not empty
            for polozka in polozky_to_associate:
                # Create association only if there are Polozka instances to associate
                PolozkaPobocka.objects.get_or_create(polozka=polozka, pobocka=self)

    def __str__(self):
        return self.pobocka_name

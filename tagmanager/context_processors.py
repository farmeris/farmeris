
# tagmanager/context_processors.py

from .forms import TagRequestForm

def tag_request_form(request):
    if request.user.is_authenticated:
        return {'tag_request_form': TagRequestForm()}
    return {}

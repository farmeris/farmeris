
# tagmanager/models.py

from django.db import models
from django.utils.text import slugify
from django.conf import settings

class Tag(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_approved = models.BooleanField(default=False)
    alias_for = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True, related_name='aliases')

    def save(self, *args, **kwargs):
        self.name = self.name.lower().strip()  # Normalize the name
        self.slug = slugify(self.name)
        super(Tag, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


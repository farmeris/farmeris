
# tagmanager/urls.py
#
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from tagmanager import views

app_name = 'tagamanger'

urlpatterns = [
    path('request-tag/', views.request_tag, name='request_tag'),
    path('<str:username>/add-tags', views.select_tag, name='select_tag'),
]

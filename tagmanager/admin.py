
# tagmanager/admin.py

from django.contrib import admin
from .models import Tag

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    search_fields = ['name', 'slug']
    actions = ['merge_tags']

    def merge_tags(self, request, queryset):
        # Here you would implement your logic to merge tags
        pass

    merge_tags.short_description = "Merge selected tags into one"


# tagmanager/views.py

from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from decouple import config
from tagmanager.forms import TagSelectionForm2, TagSelectionForm3, TagRequestForm
from login_app.models import UserProfileTag
from tagmanager.models import Tag
# from add_product_main.models import Polozka
from login_app.models import UserProfile
from django.contrib import messages
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

@login_required
def request_tag(request):
    # user_profile = UserProfile.objects.get(user=request.user)
    if request.method == 'POST':
        form = TagRequestForm(request.POST)
        if form.is_valid():
            tag_name = form.cleaned_data['tag_name']
            new_tag = Tag.objects.create(name=tag_name, is_approved=request.user.is_superuser)

            if request.user.is_superuser:
                messages.success(request, "Tag added and approved successfully.")
            else:
                # Send an email to admin for tag approval
                subject = "New Tag Request"
                email_message = f"Tag Request by {request.user.username}, Tag Name: {tag_name}"
                email_from = settings.DEFAULT_FROM_EMAIL
                recipient_list = settings.EMAIL_RECIPIENT_LIST.split(',')
                send_mail(subject, email_message, email_from, recipient_list)
                messages.info(request, "Tag request submitted for review.")

            return redirect('tagmanager:request_tag')
    else:
        form = TagRequestForm()

    context = {
        'form': form
    }
    return render(request, 'request_tag.html', context)

@login_required
def select_tag(request, username):
    # Ensure the user exists and get their UserProfile
    user = get_object_or_404(User, username=username)
    user_profile = UserProfile.objects.get(user=user)

    if request.method == 'POST':
        form = TagSelectionForm2(request.POST)
        if form.is_valid():
            selected_tag = form.cleaned_data['tag']
            UserProfileTag.objects.create(user_profile=user_profile, tag=selected_tag)
            messages.success(request, "Tag added successfully! Add another if you like.")
            return redirect('tagmanager:select_tag', username=username)  # Redirect back to the same page

    else:
        form = TagSelectionForm2()

    referrer = request.META.get('HTTP_REFERER', request.build_absolute_uri(reverse('main:main')))
    return render(request, 'select_tag.html', {'form': form, 'user': user, 'referrer': referrer})

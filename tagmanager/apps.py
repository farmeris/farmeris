
# tagmanager/apps.py

from django.apps import AppConfig

class TagmanagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tagmanager'

    def ready(self):
        from . import signals


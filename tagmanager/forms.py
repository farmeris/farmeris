
# tagmanager/forms.py

from django import forms
from tagmanager.models import Tag
from login_app.models import UserProfileTag

class TagRequestForm(forms.Form):
    tag_name = forms.CharField(label='Enter Tag Name', max_length=255, widget=forms.TextInput(attrs={'placeholder': 'Tag Name'}))

class TagSelectionForm2(forms.Form):
    tag = forms.ModelChoiceField(
        queryset=Tag.objects.filter(is_approved=True).order_by('name'),
        label='Select a Tag',
        # help_text='Choose from the list of approved tags.',
        empty_label=None
    )

class TagSelectionForm3(forms.ModelForm):
    class Meta:
        model = UserProfileTag
        fields = ('tag',)

    def __init__(self, *args, **kwargs):
        user_profile = kwargs.pop('user_profile', None)
        super(TagSelectForm, self).__init__(*args, **kwargs)
        if user_profile:
            self.fields['tag'].queryset = Tag.objects.filter(
                tagged_profiles__user_profile=user_profile
            ).distinct() | Tag.objects.filter(is_approved=True)


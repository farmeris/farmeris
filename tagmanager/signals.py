
# tagmanager/signals.py

from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Tag


@receiver(post_save, sender=Tag)
def auto_delete_unapproved_tag(sender, instance, created, **kwargs):
    if not instance.is_approved and not created:
        instance.delete()

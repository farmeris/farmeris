
# api/serializers/userlocations_serializers.py

from add_product_main.models import Polozka, PolozkaPobocka
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from edit_profile.models import Pobocka
from login_app.models import UserProfile
from rest_framework import serializers

User = get_user_model()

# locations serializers
class UserPobockaSerializer(serializers.ModelSerializer):
    # Serializer for 'Pobocka' used within the 'UserLocationsSerializer'.
    # It simplifies the Pobocka representation focusing on location data.
    location_pobocka = serializers.SerializerMethodField()

    def get_location_pobocka(self, obj):
        if obj.location_pobocka:
            return {
                'latitude': obj.location_pobocka.y,
                'longitude': obj.location_pobocka.x
            }
        return None

    class Meta:
        model = Pobocka
        fields = ['city', 'state_province', 'zip_postal_code', 'country',
                  'location_pobocka', 'pobocka_name']


class SearchUserLocationsSerializer(serializers.ModelSerializer):
    """
    Serializer for searching and returning user location data based on a search query.

    This serializer is used to filter and serialize user profile data that includes
    location information tied to specific user queries.
    """
    pobocky = serializers.SerializerMethodField()
    username = serializers.CharField(source='user.username')

    def get_pobocky(self, obj):
        # Using prefetched filtered_pobocky if available, otherwise defaulting to all pobocky
        if hasattr(obj, 'filtered_pobocky'):
            return UserPobockaSerializer(obj.filtered_pobocky, many=True).data
        else:
            return UserPobockaSerializer(obj.pobocky.all(), many=True).data

    class Meta:
        model = UserProfile
        fields = ['username', 'avatar', 'website', 'facebook', 'twitter', 'instagram', 'tiktok', 'youtube',
                  'full_mobile_number', 'languages',
                  'company_name', 'company_id_number', 'tax_id_number',
                  'vat_number', 'company_address', 'pobocky']

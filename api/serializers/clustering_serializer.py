
from rest_framework import serializers


class ClusterSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    centroid = serializers.SerializerMethodField()

    def get_centroid(self, obj):
        return {
            'latitude': obj['centroid'].centroid.y,
            'longitude': obj['centroid'].centroid.x
        }

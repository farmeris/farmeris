
# api/models.py

from django.contrib.gis.db import models

class Region(models.Model):
    name = models.CharField(max_length=255)
    geo_boundary = models.PolygonField()
    # Additional fields can be added here

    def __str__(self):
        return self.name

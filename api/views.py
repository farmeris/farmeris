
#api/views.py

from add_product_main.models import Polozka
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.postgres.search import (SearchQuery, SearchRank,
                                            SearchVector)
from django.db.models import (Case, Exists, IntegerField, OuterRef, Prefetch,
                              Q, Value, When)
from django.db.models.expressions import Func, Value
from django.db.models.functions import Lower
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import (CharFilter, DjangoFilterBackend,
                                           FilterSet)
from edit_profile.models import Pobocka
from login_app.models import UserProfile
from rest_framework import generics, status, views, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from api.serializers import (ClusterSerializer, SearchUserLocationsSerializer,
                             UserPobockaSerializer)
from django.db import connection
from rest_framework.views import APIView
from django.contrib.gis.geos import fromstr
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point, MultiPoint
from django.contrib.gis.db.models.functions import Centroid
from django.db.models import Count
from api.models import Region
import json
import logging

User = get_user_model()
logger = logging.getLogger('django.debug')


# def normalize_name(name):
#     if 'Prešov' in name and 'kraj' not in name:
#         name = 'Prešovský kraj'
#     if 'Trenciansk' in name:
#         name = 'Trenčiansky kraj'
#     elif 'kraj' not in name:
#         name += ' kraj'  # Append 'kraj' if not already present
#     return name


def normalize_name(name):
    region_mappings = {
        'Banskobystrický': 'Banskobystrický kraj',
        'Bratislavský': 'Bratislavský kraj',
        'Košický': 'Košický kraj',
        'Nitriansky': 'Nitriansky kraj',
        'Prešov': 'Prešovský kraj',  # Include common misspellings or shorthand
        'Trenciansky': 'Trenčiansky kraj',  # Account for missing accents or suffixes
        'Trnavský': 'Trnavský kraj',
        'Žilinský': 'Žilinský kraj'
    }
    return region_mappings.get(name, name)


class UserProfileFilter(FilterSet):
    '''
    Filter for fields to query for in the main API
    '''
    pobocka_name = CharFilter(method='filter_pobocka_name')

    class Meta:
        model = UserProfile
        fields = []

    def filter_pobocka_name(self, queryset, name, value):
        if value:
            queryset = queryset.annotate(
                search_vector=SearchVector(
                    'pobocky__pobocka_name', 'pobocky__city',
                    'pobocky__state_province', 'pobocky__country'
                ),
                has_pobocka=Case(
                    When(pobocky__pobocka_name__icontains=value, then=Value(1)),
                    default=Value(0),
                    output_field=IntegerField()
                )
            ).filter(search_vector=SearchQuery(value)).distinct('id')
            queryset = queryset.prefetch_related(
                Prefetch('pobocky', queryset=Pobocka.objects.filter(
                    Q(pobocka_name__icontains=value) | Q(city__icontains=value) | Q(state_province__icontains=value) | Q(country__icontains=value)),
                    to_attr='filtered_pobocky')
            )
        return queryset

class UserLocationsViewSet(viewsets.ReadOnlyModelViewSet):
    '''
    Main API displaying main map data with query
    '''
    serializer_class = SearchUserLocationsSerializer
    queryset = UserProfile.objects.filter(is_trusted=True, is_seller=True).distinct('id')
    filter_backends = (DjangoFilterBackend,)
    filterset_class = UserProfileFilter

    def get_queryset(self):
        queryset = super().get_queryset()
        query = self.request.query_params.get('query', '').strip()

        if query:
            search_vectors = SearchVector(
                'user__username', 'full_mobile_number', 'company_name',
                'pobocky__pobocka_name', 'pobocky__city', 'pobocky__state_province',
                'pobocky__country', 'pobocky__zip_postal_code', 'pobocky__location_pobocka'
            )
            search_query = SearchQuery(query)
            queryset = queryset.annotate(search=search_vectors).filter(search=search_query).distinct('id')

            # Prefetch only relevant pobocky based on the search query
            pobocka_filter = Q(pobocka_name__icontains=query) | Q(city__icontains=query) | Q(state_province__icontains=query) | Q(country__icontains=query)
            queryset = queryset.prefetch_related(
                Prefetch('pobocky', queryset=Pobocka.objects.filter(pobocka_filter), to_attr='filtered_pobocky')
            )

        return queryset


class ClusterAPIView(APIView):
    '''
    First try to implement clustering API
    '''
    def get(self, request):
        radius = D(km=300)  # Define the radius for clustering within each city
        clusters = []

        # Iterate over each unique city or region
        for city in Pobocka.objects.values_list('city', flat=True).distinct():
            # Filter locations in this city
            city_locations = Pobocka.objects.filter(city=city, location_pobocka__isnull=False)
            if city_locations.exists():
                multipoint = MultiPoint([loc.location_pobocka for loc in city_locations])
                centroid = multipoint.centroid
                cluster_data = {
                    "city": city,
                    "centroid": [centroid.x, centroid.y],
                    "count": city_locations.count(),
                    "locations": [
                        {
                            "id": loc.id,
                            "address": loc.street_address,
                            "coordinates": [loc.location_pobocka.x, loc.location_pobocka.y]
                        } for loc in city_locations
                    ]
                }
                clusters.append(cluster_data)

        return Response({"clusters": clusters})

class StateProvincePobockaCountViewSet(ReadOnlyModelViewSet):
    '''
    Clustering API for regions - state_province variable counter, not doing actual clustering, only counting
    '''
    queryset = Pobocka.objects.all()
    http_method_names = ['get']

    def list(self, request, *args, **kwargs):
        # Fetch regions and normalize names
        regions = Region.objects.annotate(centroid=Centroid('geo_boundary')).all()
        region_data = {}
        for region in regions:
            norm_name = normalize_name(region.name)
            region_data[norm_name] = {
                "centroid": {"latitude": region.centroid.y, "longitude": region.centroid.x},
                "geo_boundary": region.geo_boundary.geojson,
                "count": 0
            }

        query = request.query_params.get('query', None)
        filtered_queryset = self.queryset.filter(
            Q(city__icontains=query) |
            Q(state_province__icontains=query) |
            Q(country__icontains=query) |
            Q(pobocka_name__icontains=query)
        ) if query else self.queryset

        region_counts = filtered_queryset.values('state_province').annotate(count=Count('id')).order_by('state_province')

        for region_count in region_counts:
            norm_region_name = normalize_name(region_count['state_province'])
            if norm_region_name in region_data:
                region_data[norm_region_name]['count'] = region_count['count']
            else:
                logger.error(f"No matching region found for normalized name: {norm_region_name}")

        results = [{
            "state_province": name,
            "count": data['count'],
            "centroid": data['centroid'],
            "geo_boundary": data['geo_boundary']
        } for name, data in region_data.items()]
        return Response(results)

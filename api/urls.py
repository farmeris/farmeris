
#api/urls.py

from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from api.views import ClusterAPIView, UserLocationsViewSet, StateProvincePobockaCountViewSet

app_name = 'api'

router = DefaultRouter()
router.register(r'locations', UserLocationsViewSet, basename='locations')
router.register(r'regions', StateProvincePobockaCountViewSet, basename='state-province-count')

urlpatterns = [
    path('', include(router.urls)),
    path('clusters/', ClusterAPIView.as_view(), name='cluster-api'),  # Use path directly
]

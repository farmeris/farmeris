
# texts/urls.py

from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from texts import views

app_name = 'texts'

urlpatterns = [
    path('raw_milk_manifesto/', views.manifesto, name='manifesto'),
    path('about/', views.about, name='about'),
    path('contact/', views.contact, name='contact'),
    path('tutorial/', views.uncoded, name='tutorial'),
    path('thank-you/', views.thanks, name='thank_you'),
]

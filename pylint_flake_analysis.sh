#!/bin/bash

# Generate a comma-separated list of exclusions for flake8
exclude_list=$(cat .gitignore | sed '/^\s*$/d' | sed '/^#/d' | tr '\n' ',' | sed 's/,$//')
echo "Exclusion list: $exclude_list"

# Run flake8 with exclusions
flake8 . --exclude=$exclude_list > flake8_output.txt
echo "flake8 analysis complete, output in flake8_output.txt"

# For pylint, we need to find all python files not in the excluded directories and run pylint on them
find . -type f -name "*.py" | grep -vE "$(echo $exclude_list | tr ',' '|')" | xargs pylint > pylint_output.txt
echo "pylint analysis complete, output in pylint_output.txt"

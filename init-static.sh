#!/bin/bash
# init-static.sh

# Define absolute path to your Django project
PROJECT_PATH="/home/kekw/Documents/programming/farmeris"

# Define the Python interpreter path
PYTHON_PATH="/home/kekw/Documents/programming/farmeris/test_venv/bin/python"

# Remove existing static files
sudo rm -rf $PROJECT_PATH/staticfiles/*
sudo rm -rf $PROJECT_PATH/static/*

# Collect static files
$PYTHON_PATH $PROJECT_PATH/manage.py collectstatic --noinput

# Check if staticfiles directory exists and is not empty
if [ "$(ls -A $PROJECT_PATH/staticfiles)" ]; then
   # Copy static files to the desired location
   sudo cp -r $PROJECT_PATH/staticfiles/* $PROJECT_PATH/static/
else
   echo "No static files to copy."
fi


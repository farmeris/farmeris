from functools import wraps

from allauth.account.models import EmailAddress
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.signing import BadSignature, SignatureExpired, loads
from django.shortcuts import redirect

# def email_verification_required(view_func):
#     @wraps(view_func)
#     def _wrapped_view(request, *args, **kwargs):
#         # Check if the email_verified session variable is set
#         if request.session.get('email_verified'):
#             return view_func(request, *args, **kwargs)
#         else:
#             messages.error(request, "You need to verify your email address to access this page.")
#             return redirect('send_verification_email')
#     return _wrapped_view



def email_verification_required(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        # Ensure the user is authenticated
        if not request.user.is_authenticated:
            messages.error(request, "You must be logged in to access this page.")
            return redirect('account_login')

        # Check if the user has at least one verified email
        user_has_verified_email = EmailAddress.objects.filter(user=request.user, verified=True).exists()

        if user_has_verified_email:
            # If the user has verified email(s), check if the session variable is set
            if request.session.get('email_verified'):
                # If verified in this session, proceed to the view
                return view_func(request, *args, **kwargs)
            else:
                # If not verified in this session, redirect to verification
                messages.error(request, "You need to verify your email address to access this page.")
                return redirect('send_verification_email')
        else:
            # If the user doesn't have any verified emails, allow access without further checks
            return view_func(request, *args, **kwargs)

    return _wrapped_view

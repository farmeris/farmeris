document.addEventListener('DOMContentLoaded', function() {
    var checkbox = document.getElementById('select-individual');
    var toggledDiv = document.querySelector('.select-individual-toggled');
    var notToggledDiv = document.querySelector('.select-individual-not-toggled');

    // Function to toggle the display based on the checkbox
    function toggleDisplay() {
        if (checkbox.checked) {
            toggledDiv.style.display = 'flex';
            notToggledDiv.style.display = 'none';
        } else {
            toggledDiv.style.display = 'none';
            notToggledDiv.style.display = 'flex';
        }
    }

    // Initial check to set the correct display on page load
    toggleDisplay();

    // Add event listener to change display when the checkbox state changes
    checkbox.addEventListener('change', toggleDisplay);
});

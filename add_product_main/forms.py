
#add_product_main/forms.py

from django import forms
from django.core.validators import FileExtensionValidator
from djmoney.models.fields import MoneyField
from login_app.constants import CURRENCY_CHOICES
from login_app.models import UserProfile
from user_profile.forms import UserProfileForm

from .models import Cena, Polozka, PolozkaTransport, Transport


class PictureForm(forms.Form):
    image = forms.ImageField(validators=[FileExtensionValidator(allowed_extensions=['jpg', 'jpeg', 'png', 'gif'])])

class PolozkaForm(forms.ModelForm):
    class Meta:
        model = Polozka
        fields = ['nazov_produktu', 'info_nazov_produktu', 'info_produktu', 'image', 'associate_all_pobocky']

class CenaForm(forms.ModelForm):
    price = MoneyField(max_digits=14, decimal_places=4, currency_choices=CURRENCY_CHOICES, default_currency='EUR')

    class Meta:
        model = Cena
        fields = ['amount', 'unit', 'price']

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(CenaForm, self).__init__(*args, **kwargs)
        if user:
            user_profile = UserProfile.objects.get(user=user)
            self.fields['price'].currency = user_profile.preferred_currency

class TransportForm(forms.ModelForm):
    price = MoneyField(max_digits=14, decimal_places=4, currency_choices=CURRENCY_CHOICES, default_currency='EUR')

    class Meta:
        model = Transport
        fields = ['transport_type', 'transport_notes', 'amount', 'unit', 'price']

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(CenaForm, self).__init__(*args, **kwargs)
        if user:
            user_profile = UserProfile.objects.get(user=user)
            self.fields['price'].currency = user_profile.preferred_currency

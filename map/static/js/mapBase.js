
// mapBase.js

var view;
var baseLayer, locationsLayer, regionsLayer;

function initializeMap() {
    view = new ol.View({
        center: ol.proj.fromLonLat([19.5, 48.7]),
        zoom: 7,
        minZoom: 3,
        maxZoom: 18,
        enableRotation: false
    });

    // Create the base tile layer using OpenLayers' OSM source
    baseLayer = new ol.layer.Tile({
        source: new ol.source.OSM()
    });

    locationsLayer = new ol.layer.Vector({
        source: new ol.source.Vector(),
        visible: false
    });

    regionsLayer = new ol.layer.Vector({
        source: new ol.source.Vector(),  // Separate vector source for regions
        visible: true
    });

    const regionStyle = new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.6)'  // light white fill
        }),
        stroke: new ol.style.Stroke({
            color: '#319FD3', // blue borders
            width: 1
        })
    });

    const highlightRegionStyle = new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 0, 0.6)'  // yellow fill on hover
        }),
        stroke: new ol.style.Stroke({
            color: '#d31900', // red borders
            width: 2
        })
    });

    // Setup the map with default controls and additional layers
    var map = new ol.Map({
        target: 'map',
        layers: [baseLayer, locationsLayer, regionsLayer],
        view: view,
        controls: ol.control.defaults({
            attribution: true,
            attributionOptions: {
                collapsible: true
            }
        }).extend([
            new ol.control.FullScreen(),
            new ol.control.ScaleLine(),
            new ol.control.OverviewMap({
                layers: [new ol.layer.Tile({
                    source: new ol.source.OSM()
                })],
                collapseLabel: '\u00BB',
                label: '\u00AB',
                collapsed: true
            }),
            new ol.control.Zoom()
        ])
    });

    // Listen for zoom level changes
    view.on('change:resolution', function() {
        var zoom = view.getZoom();
        if (zoom < 8) {
            locationsLayer.setVisible(false);
            regionsLayer.setVisible(true);
        } else {
            locationsLayer.setVisible(true);
            regionsLayer.setVisible(false);
        }
    });

    return map;
}

window.view = view;
window.baseLayer = baseLayer;
window.locationsLayer = locationsLayer;
window.regionsLayer = regionsLayer;
window.initializeMap = initializeMap;

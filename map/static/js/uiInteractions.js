
// uiInteractions.js

function setupTooltipInteraction(map, overlay) {
    // This tracks if the mouse is over the tooltip to keep it open.
    let isMouseOverTooltip = false;
    let selectedFeature = null;

    // Event listener for mouse entering and leaving the tooltip area.
    document.getElementById('info-map').addEventListener('mouseenter', () => isMouseOverTooltip = true);
    document.getElementById('info-map').addEventListener('mouseleave', () => {
        isMouseOverTooltip = false;
        if (!selectedFeature) overlay.getElement().style.display = 'none';
    });

    // Display the tooltip on pointer move but skip during map dragging.
    map.on('pointermove', function(evt) {
        if (evt.dragging) return;
        const pixel = map.getEventPixel(evt.originalEvent);
        const hit = map.hasFeatureAtPixel(pixel, {
            layerFilter: function(layer) {
                return layer.getVisible();  // Only consider features from visible layers
            }
        });

        if (hit && !isMouseOverTooltip && !selectedFeature) {
            map.forEachFeatureAtPixel(pixel, function(feature, layer) {
                if (layer === locationsLayer || layer === regionsLayer) {  // Ensure the feature is from a relevant layer
                    displayTooltip(feature);
                    return true;  // Stops after the first feature is found
                }
            }, {
                layerFilter: function(layer) {
                    return layer.getVisible();  // Again, ensure feature is from a visible layer
                }
            });
        } else if (!isMouseOverTooltip && !selectedFeature) {
            overlay.getElement().style.display = 'none';  // Hide the tooltip if no relevant feature is found
        }
    });

    // Lock the tooltip open on a feature click.
    map.on('click', function(evt) {
        let featureFound = false;

        map.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
            if (layer === locationsLayer) {
                selectedFeature = feature;  // Only set selectedFeature if the click is on a location feature
                displayTooltip(selectedFeature);
                featureFound = true;
                return true;
            }
        }, {
            layerFilter: (layer) => layer === locationsLayer
            /**
             * This code ensures the click happens only on a proper layer, therefore eliminating tooltip display bug on click on region layers - maybe needed in a future clustering?
             **/
        });

        if (!featureFound) {
            selectedFeature = null;  // Reset selectedFeature if no feature is found
            overlay.getElement().style.display = 'none';  // Hide the tooltip
        }
    });

    function displayTooltip(feature) {
        const userData = feature.getProperties().userData;
        if (userData) {
            fillTooltip(userData);
            overlay.setPosition(feature.getGeometry().getCoordinates());
            overlay.getElement().style.display = '';
        }
    }
}

function fillTooltip(userData) {
    updateUsernameLink('username-link', userData.username, `/${userData.username}/`);
    updateField('phone', userData.phone);
    updateField('website', userData.website, userData.website); // Pass the website URL here if it's a link
    updateField('company', userData.companyName);
    updateField('state', userData.stateProvince);
}

function updateField(fieldId, content, linkHref = null) {
    const element = document.getElementById(fieldId);
    const contentElement = linkHref ? element.querySelector('a') : element.querySelector('span');

    if (content) {
        if (linkHref) {
            if (!contentElement) {
                let newLink = document.createElement('a');
                newLink.target = "_blank";  // Open in a new tab
                element.appendChild(newLink);
                contentElement = newLink;
            }
            contentElement.href = linkHref;
            contentElement.textContent = content;
        } else {
            if (!contentElement) {
                let newSpan = document.createElement('span');
                element.appendChild(newSpan);
                contentElement = newSpan;
            }
            contentElement.textContent = content;
        }
        element.style.display = 'block';
    } else {
        element.style.display = 'none';
    }
}

function updateUsernameLink(linkId, username, userProfilePath) {
    const linkElement = document.getElementById(linkId);
    const usernameDiv = document.getElementById('username'); // Get the div inside the link

    if (username) {
        linkElement.href = userProfilePath; // Set the link's href to the userProfilePath
        usernameDiv.textContent = username; // Set the div's text content to the username
        linkElement.style.display = 'block'; // Ensure the link is visible
    } else {
        linkElement.style.display = 'none'; // Hide the link if there is no username
    }
}


window.setupTooltipInteraction = setupTooltipInteraction;

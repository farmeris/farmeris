
// showHide.js

function hideAllMarkers() {
    if (window.locationsLayer) {
        window.locationsLayer.setVisible(false);
        window.locationsLayer.set('interactive', false);  // Disable interactions
    }
}

function showAllMarkers() {
    if (window.locationsLayer) {
        window.locationsLayer.setVisible(true);
        window.locationsLayer.set('interactive', true);  // Re-enable interactions
    }
}

function showRegions() {
    if (window.regionsLayer) {
        window.regionsLayer.setVisible(true);
        window.regionsLayer.set('interactive', true);  // Assume you want to interact with regions
    }
}

function hideRegions() {
    if (window.regionsLayer) {
        window.regionsLayer.setVisible(false);
        window.regionsLayer.set('interactive', false);  // Disable interactions
    }
}

window.hideAllMarkers = hideAllMarkers;
window.showAllMarkers = showAllMarkers;
window.showRegions = showRegions;
window.hideRegions = hideRegions;

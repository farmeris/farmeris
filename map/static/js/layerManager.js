
// layerManager.js

const defaultStyle = new ol.style.Style({
    image: new ol.style.Icon({
        anchor: [0.5, 15],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '/static/media/map-pin-filled2-red.svg',
        scale: 1.5
    })
});

const highlightedStyle = new ol.style.Style({
    image: new ol.style.Icon({
        anchor: [0.5, 15],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '/static/media/map-pin-filled2-blue.svg',
        scale: 1.5
    })
});

const queriedStyle = new ol.style.Style({
    image: new ol.style.Icon({
        anchor: [0.5, 15],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '/static/media/map-pin-filled2-yellow.svg',
        scale: 1.5
    })
});

const queriedHighlightedStyle = new ol.style.Style({
    image: new ol.style.Icon({
        anchor: [0.5, 15],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '/static/media/map-pin-filled2-blue.svg',
        scale: 1.5
    })
});


let lastHoveredFeature = null;
let focusedFeature = null;
let moveTimeout = null;
let focusedFeatureLayer = null;


function addLocationsToMap(map, users, query) {
    locationsLayer.getSource().clear();  // Clear previous markers
    const vectorSource = locationsLayer.getSource();

    users.forEach(user => {
        user.pobocky.forEach(pobocka => {
            if (pobocka.location_pobocka) {
                const feature = new ol.Feature({
                    geometry: new ol.geom.Point(ol.proj.fromLonLat([pobocka.location_pobocka.longitude, pobocka.location_pobocka.latitude])),
                    name: user.username
                });
                feature.setProperties({
                    userData: {
                        username: user.username,
                        phone: user.full_mobile_number,
                        website: user.website,
                        companyName: user.company_name,
                        stateProvince: pobocka.state_province
                    }
                });
                feature.setStyle(query ? queriedStyle : defaultStyle);
                vectorSource.addFeature(feature);
            }
        });
    });

    // bigger icon on hover disabled
    /*
    map.on('pointermove', function(evt) {
        if (evt.dragging) return;

        clearTimeout(moveTimeout);
        moveTimeout = setTimeout(() => {
            const pixel = map.getEventPixel(evt.originalEvent);
            const hoveredFeature = map.forEachFeatureAtPixel(pixel, function(feature) {
                return feature;
            }, {
                layerFilter: function(layer) {
                    return layer === locationsLayer;
                }
            });

            if (hoveredFeature !== lastHoveredFeature) {
                if (lastHoveredFeature) {
                    animateFeatureScale(lastHoveredFeature, 2, 1.5, 150); // Scale down when no longer hovered
                }
                if (hoveredFeature) {
                    animateFeatureScale(hoveredFeature, 1.5, 2, 150); // Scale up when hovered
                }
                lastHoveredFeature = hoveredFeature;
            }
        }, 10);
    });

    map.on('pointerout', function(evt) {
    if (lastHoveredFeature) {
    animateFeatureScale(lastHoveredFeature, 2, 1.5, 150); // Scale down when mouse leaves map
    lastHoveredFeature = null;
    }
    });
    */

    map.on('singleclick', function(evt) {
        const clickedFeature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
            return feature;
        }, {
            layerFilter: function(layer) {
                return layer === locationsLayer;
            }
        });

        if (clickedFeature !== focusedFeature) {
            if (focusedFeature) {
                focusedFeature.setStyle(query ? queriedStyle : defaultStyle); // Reset the previous focused feature
                map.removeLayer(focusedFeatureLayer); // Remove the previous focused feature layer
            }
            focusedFeature = clickedFeature;
            if (focusedFeature) {
                focusedFeature.setStyle(query ? queriedHighlightedStyle : highlightedStyle); // Highlight the new focused feature
                focusedFeatureLayer = new ol.layer.Vector({
                    source: new ol.source.Vector({
                        features: [focusedFeature]
                    }),
                    zIndex: 1 // Set the zIndex of the focused feature layer to 1
                });
                map.addLayer(focusedFeatureLayer); // Add the focused feature layer to the map
            }
        } else if (focusedFeature) {
            focusedFeature.setStyle(query ? queriedStyle : defaultStyle); // Reset if the same feature is clicked again
            map.removeLayer(focusedFeatureLayer); // Remove the focused feature layer
            focusedFeature = null;
        }
    });

}

function addRegionsToMap(map, regions) {
    const vectorSource = regionsLayer.getSource();
    vectorSource.clear();  // Clear existing regions from the source

    // Define the normal style with only the numeric count displayed
    const normalStyle = function(feature) {
        return new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#ffcc33',
                width: 2
            }),
            fill: new ol.style.Fill({
                color: 'rgba(255, 204, 51, 0.4)'  // semi-transparent fill
            }),
            text: new ol.style.Text({
                font: '14px Calibri,sans-serif',
                fill: new ol.style.Fill({ color: '#000' }),
                stroke: new ol.style.Stroke({
                    color: '#fff', width: 2
                }),
                // text: `${feature.get('countText').match(/\d+/)[0]}\n${feature.get('nameText')}`,
                text: feature.get('countText'),
                textAlign: 'center',
                overflow: true
            })
        });
    };

    // Define the hover style with only the numeric count displayed
    const hoverStyle = function(feature) {
        return new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#ffcc33',
                width: 2
            }),
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 0, 0.6)'  // bright yellow fill on hover
            }),
            text: new ol.style.Text({
                font: '14px Calibri,sans-serif',
                fill: new ol.style.Fill({ color: '#000' }),
                stroke: new ol.style.Stroke({
                    color: '#fff', width: 2
                }),
                // text: feature.get('countText'),  // Display only the numeric count even on hover
                text: `${feature.get('countText').match(/\d+/)[0]}\n${feature.get('nameText')}`,
                // text: feature.get('nameText'),
                textAlign: 'center',
                overflow: true
            })
        });
    };

    let lastHoveredFeature = null;  // Track the last hovered feature

    regions.forEach(region => {
        if (region.geo_boundary) {
            const geojsonObject = JSON.parse(region.geo_boundary);
            const feature = new ol.Feature({
                geometry: new ol.format.GeoJSON().readGeometry(geojsonObject, {
                    dataProjection: 'EPSG:4326',
                    featureProjection: map.getView().getProjection()
                }),
                nameText: region.state_province,
                countText: `${region.count}`  // Store only the numeric count
            });

            feature.setStyle(normalStyle(feature));  // Set initial style
            vectorSource.addFeature(feature);
        }
    });

    // Hover over the region to display region details
    map.on('pointermove', function(e) {
        if (e.dragging) return;
        const pixel = map.getEventPixel(e.originalEvent);
        const hit = map.hasFeatureAtPixel(pixel);
        // cursor is set to pointer when hovering the region
        map.getTargetElement().style.cursor = hit ? 'pointer' : '';

        const feature = map.forEachFeatureAtPixel(pixel, function(feature) {
            return feature;
        }, {
            layerFilter: function(layer) {
                return layer === regionsLayer;
            }
        });

        if (feature !== lastHoveredFeature) {
            if (lastHoveredFeature) {
                lastHoveredFeature.setStyle(normalStyle(lastHoveredFeature));  // Reset style of previously hovered feature
            }
            if (feature) {
                feature.setStyle(hoverStyle(feature));  // Apply hover style to new hovered feature
            }
            lastHoveredFeature = feature;  // Update the last hovered feature
        }
    });

    // Handle click event on the map to zoom and center on the region
    // FLYTO function to zoom into the region
    map.on('singleclick', function(evt) {
        const feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
            return feature;
        }, {
            layerFilter: function(layer) {
                return layer === regionsLayer;
            }
        });

        if (feature) {

            const geometry = feature.getGeometry();
            const extent = geometry.getExtent();
            const center = ol.extent.getCenter(extent);

            // Smoothly zoom and pan the map to the center of the clicked region
            map.getView().animate({
                center: center,
                zoom: Math.min(map.getView().getZoom() + 2, 12),  // Increase zoom level by 1, up to a maximum of 12
                duration: 1000  // Duration of the animation in milliseconds
            });
        }
    });


    map.getView().fit(vectorSource.getExtent(), {
        padding: [50, 50, 50, 50],
        maxZoom: 12
    });

}

window.addRegionsToMap = addRegionsToMap;
window.addLocationsToMap = addLocationsToMap;

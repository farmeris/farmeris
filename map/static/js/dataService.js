
//dataService.js

function fetchUserLocations(query) {
    let url = '/api/locations/';
    if (query) {
        url += `?query=${encodeURIComponent(query)}`;
    }
    return fetch(url)
        .then(response => response.json())
        .catch(error => console.error('Error fetching search locations:', error));
}

function fetchRegionData(query) {
    let url = '/api/regions/';
    if (query) {
        url += `?query=${encodeURIComponent(query)}`;
    }
    return fetch(url)
        .then(response => response.json())
        .catch(error => console.error('Error fetching search locations:', error));
}

window.fetchUserLocations = fetchUserLocations;
window.fetchRegionData = fetchRegionData;

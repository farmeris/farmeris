
// initMap.js

var isQueriedData = false;

document.addEventListener('DOMContentLoaded', function() {
    var map = window.initializeMap();
    var overlay = new ol.Overlay({
        element: document.getElementById('info-map'),
        positioning: 'bottom-center',
        stopEvent: true,
        offset: [0, -20]
    });
    map.addOverlay(overlay);

    // Extract the 'query' parameter from the URL
    const queryParams = new URLSearchParams(window.location.search);
    const query = queryParams.get('query');
    isQueriedData = !!query;

    // Simultaneous fetch for user and region data
    Promise.all([
        window.fetchUserLocations(query),
        window.fetchRegionData(query)
    ]).then(([users, regions]) => {
        window.addLocationsToMap(map, users, query);
        window.addRegionsToMap(map, regions);
        window.setupTooltipInteraction(map, overlay);

        // Ensure at least one user with a valid location is present
        if (query && users.length > 0 && users[0].pobocky && users[0].pobocky.length > 0) {
            const firstUserLocation = ol.proj.fromLonLat([
                users[0].pobocky[0].location_pobocka.longitude,
                users[0].pobocky[0].location_pobocka.latitude
            ]);
            // Use the flyTo function to animate map transition
            window.flyTo(firstUserLocation, function() {
                console.log('Animation to queried location complete.');
            });
        }
    }).catch(error => {
        console.error("Failed to load data:", error);
        alert("Failed to load data, please try again later.");
    });
});

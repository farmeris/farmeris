
// animationManager.js

const lastAnimatedTime = new Map();  // Keeps track of the last animation time for each feature

function animateFeatureScale(feature, startScale, endScale, duration, forceComplete = false) {
    if (feature === focusedFeature && !forceComplete) return;  // Skip animation if feature is focused and not forced

    const currentTime = Date.now();
    const lastTime = lastAnimatedTime.get(feature) || 0;

    if (currentTime - lastTime < duration && !forceComplete) return;  // Ensure animations do not overlap, unless forced
    lastAnimatedTime.set(feature, currentTime);

    let currentScale = startScale;

    const animation = () => {
        currentScale += (endScale > startScale ? 0.05 : -0.05);
        if (feature !== focusedFeature || forceComplete) {
            feature.setStyle(new ol.style.Style({
                image: new ol.style.Icon({
                    src: isQueriedData ? '/static/media/map-pin-filled2-yellow.svg' : '/static/media/map-pin-filled2-red.svg',
                    scale: currentScale,
                    anchor: [0.5, 15],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    zIndex: 100
                })
            }));
        }

        if (((endScale > startScale && currentScale < endScale) || (endScale < startScale && currentScale > endScale)) && !forceComplete) {
            requestAnimationFrame(animation);
        } else if (forceComplete) {
            // If force complete, set the end scale directly to ensure it finishes correctly
            feature.setStyle(new ol.style.Style({
                image: new ol.style.Icon({
                    src: isQueriedData ? '/static/media/map-pin-filled2-yellow.svg' : '/static/media/map-pin-filled2-red.svg',
                    scale: endScale,
                    anchor: [0.5, 15],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    zIndex: 100
                })
            }));
        }
    };

    requestAnimationFrame(animation);
}

window.animateFeatureScale = animateFeatureScale;

function flyTo(location, done) {
    const moveDuration = 1500;  // Shorter duration for faster centering
    const zoomDuration = 1500;  // Longer duration for slower, smoother zoom
    const zoom = window.view.getZoom();
    let called = false;

    function callback(complete) {
        if (called) {
            return;
        }
        if (!complete) {
            called = true;
            done(complete);
        }
    }

    // First animate to the location with the current zoom level
    view.animate(
        {
            center: location,
            duration: moveDuration,  // Use faster movement duration
        },
        callback,
    );

    // Delay the zoom animation until after the centering is finished
    setTimeout(() => {
        view.animate(
            {
                zoom: zoom + 3,
                duration: zoomDuration,  // Use slower zoom duration
            },
            callback
        );
    }, moveDuration);
}

window.flyTo = flyTo;

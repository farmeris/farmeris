class GeoRouter:
    """
    A router to control database operations for the 'map' app to the
    geospatial database and all other apps to the default database.
    """

    def db_for_read(self, model, **hints):
        """
        Routes read operations for models in the 'map' app to 'geospatial' database.
        """
        if model._meta.app_label == 'map':
            return 'geospatial'
        return 'default'

    def db_for_write(self, model, **hints):
        """
        Routes write operations for models in the 'map' app to 'geospatial' database.
        """
        if model._meta.app_label == 'map':
            return 'geospatial'
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allows relations if both objects are in the same database.
        """
        if obj1._state.db == obj2._state.db:
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Ensures that the 'map' app's models only appear in the 'geospatial' database,
        and all other apps' models appear in the default database.
        """
        if app_label == 'map':
            return db == 'geospatial'
        return db == 'default'

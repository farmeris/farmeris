
# #map/utils.py

# import requests
# from urllib.parse import urlencode

# def geocode_address_nominatim(address):
#     """Geocode an address string to latitude and longitude using Nominatim API."""
#     base_url = "https://nominatim.openstreetmap.org/search?"
#     headers = {
#         'User-Agent': 'priec@farmeris.sk',
#     }
#     # Construct query parameters
#     params = {
#         'q': address,
#         'format': 'json',
#         'limit': 1
#     }

#     try:
#         response = requests.get(f"{base_url}{urlencode(params)}", headers=headers)
#         if response.status_code == 200:
#             data = response.json()
#             if data:
#                 latitude = data[0]['lat']
#                 longitude = data[0]['lon']
#                 return latitude, longitude
#     except requests.RequestException as e:
#         print(f"Request failed: {e}")  # Consider using logging instead of print in production
#     return None, None

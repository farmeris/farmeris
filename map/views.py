
# map/views.py

from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from login_app.models import UserProfile

def show_map(request):
    query = request.GET.get('query', None)

    if 'query' in request.GET:
        request.session['user_input_query'] = query  # Store the query in session if it's a user input

    if request.user.is_authenticated:
        user_profile = UserProfile.objects.get(user=request.user)
        filter_trusted = user_profile.filter_trusted
    else:
        user_profile = None
        filter_trusted = True

    context = {
        'user_profile': user_profile,
        'current_view_name': 'map',
        'query': request.session.get('user_input_query')
    }
    return render(request, 'show_map.html', context)

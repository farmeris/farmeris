

from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path

from map import views

app_name = 'map'

urlpatterns = [
    path('map/', views.show_map, name='show_map'),
]

import re

from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError


def custom_password_validation(password):
    try:
        validate_password(password)
    except ValidationError as error:
        raise ValidationError("Zle heslo.")

# validators for models.py for social media links
def validate_facebook_url(value):
    if not value:
        return
    if not re.match(r'^https?:\/\/(www\.)?facebook\.com\/.+$', value):
        raise ValidationError('Enter a valid Facebook profile URL.')

def validate_twitter_url(value):
    if not value:
        return
    if not re.match(r'^https?:\/\/(www\.)?(twitter\.com\/.+|x\.com\/.+)$', value):
        raise ValidationError('Enter a valid Twitter profile URL.')

def validate_instagram_url(value):
    if not value:
        return
    if not re.match(r'^https?:\/\/(www\.)?instagram\.com\/.+$', value):
        raise ValidationError('Enter a valid Instagram profile URL.')

def validate_tiktok_url(value):
    if not value:
        return
    if not re.match(r'^https?:\/\/(www\.)?tiktok\.com\/@?.+$', value):
        raise ValidationError('Enter a valid TikTok profile URL.')

def validate_youtube_url(value):
    if not value:
        return
    if not re.match(r'^https?:\/\/(www\.)?youtube\.com\/(channel\/|c\/|user\/).+$', value):
        raise ValidationError('Enter a valid YouTube channel URL.')


#login_app/forms.py

from allauth.account.forms import SignupForm
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.utils.translation import gettext_lazy as _
from login_app.models import UserProfile

from .validators import custom_password_validation


class RegisterForm(UserCreationForm):
    email = forms.EmailField(required=False)

    class Meta(UserCreationForm.Meta):
        fields = ['username', 'email', 'password1', 'password2']

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if User.objects.filter(username=username).exists():
            self.add_error('username', 'A user with that username already exists.')
        return username

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if email and User.objects.filter(email=email).exists():
            self.add_error('email', 'This email address is already in use.')
        return email

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        custom_password_validation(password1)  # Use your custom password validation
        return password1

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError("Passwords do not match.")
        custom_password_validation(password2)  # Use your custom password validation
        return password2

class ThemeForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['theme']

# class SidebarVisibilityForm(forms.ModelForm):
#     class Meta:
#         model = UserProfile
#         fields = []

class CustomSignupForm(SignupForm):
    def save(self, request):
        # Save the user using the standard logic
        user = super(CustomSignupForm, self).save(request)
        # At this point, you can customize the user object if needed
        # For example, save the Google profile name to the UserProfile model
        # user.userprofile.google_name = 'Google Profile Name' # Replace with actual data
        # user.userprofile.save()
        return user

    def clean_email(self):
        email = self.cleaned_data.get('email')
        domain = email.split('@')[-1]
        if domain.lower() == 'gmail.com':
            raise ValidationError(_('Please use Google to sign up.'))  # Customize this message as needed
        return email

class SetUsernameForm(forms.Form):
    username = forms.CharField(max_length=150)

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise ValidationError("This username is already taken.")
        return username

# login_app/urls.py

from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from login_app.views import set_username

app_name = 'map'

urlpatterns = [
    path('accounts/set/username/', set_username, name='set_username'),
]

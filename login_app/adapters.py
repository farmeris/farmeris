
#login_app/adapters.py

import uuid
from mimetypes import guess_extension

import requests
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.core.files.base import ContentFile
from login_app.models import UserProfile


class CustomSocialAccountAdapter(DefaultSocialAccountAdapter):
    def save_user(self, request, sociallogin, form=None):
        user = super().save_user(request, sociallogin, form)
        user_profile, created = UserProfile.objects.get_or_create(user=user)

        # Use the `created` flag from `get_or_create` to determine if the user is new
        if created:
            request.session['new_oauth_user'] = True

        extra_data = sociallogin.account.extra_data
        # Fetch profile picture URL from Google data
        picture_url = extra_data.get('picture')
        if picture_url:
            response = requests.get(picture_url)
            if response.status_code == 200:
                content_type = response.headers['content-type']
                extension = guess_extension(content_type) or '.jpg'  # Default to .jpg if the extension cannot be guessed
                file_name = f"{uuid.uuid4()}{extension}"
                img_temp = ContentFile(response.content)
                user_profile.avatar.save(file_name, img_temp, save=True)

        # Assuming fields for 'locale' and 'gender' are added to your UserProfile model
        user_profile.google_name = extra_data.get('name', '')
        user_profile.locale = extra_data.get('locale', 'en')  # Defaulting to 'en' if not present
        user_profile.gender = extra_data.get('gender', '')  # Defaulting to an empty string if not present

        # Assign a UUID-based username with a "-google" suffix
        user.username = str(uuid.uuid4()) + "-google"

        user.save()
        user_profile.save()

        return user

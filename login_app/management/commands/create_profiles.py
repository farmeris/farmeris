
# login_app/management/commands/create_profiles.py

import csv
import os
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from allauth.account.models import EmailAddress
from login_app.models import UserProfile

class Command(BaseCommand):
    help = 'Create user profiles from a CSV computer file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='Path to the CSV file containing user profile data')

    def handle(self, *args, **options):
        file_path = options['file_path']  # Corrected the key here
        default_password = os.environ.get('DEFAULT_PASSWORD', 'fallback_default_password')

        with open(file_path, 'r') as file:
            reader = csv.DictReader(file)
            for row in reader:

                username = row['user']
                email = row.get('email', '')
                phone_country_code = row.get('phone_country_code', '')
                phone_number = row.get('phone_number', '')
                company_name = row.get('company_name', '')
                website = row.get('website', '')
                facebook = row.get('facebook', '')
                twitter = row.get('twitter', '')
                instagram = row.get('instagram', '')
                tiktok = row.get('tikook', '')
                youtube = row.get('dailymotion', '')

                user, created = User.objects.get_or_create(username=username)
                user.email = email
                user.set_password(default_password)
                user.save()

                if created and email:
                    EmailAddress.objects.create(user=user, email=email, primary=True, verified=True)

                UserProfile.objects.update_or_create(
                    user=user,
                    defaults={
                        'website': website,
                        'facebook': facebook,
                        'twitter': twitter,
                        'instagram': instagram,
                        'tikook': tiktok,
                        'dailymotion': youtube,
                        'phone_country_code': phone_country_code,
                        'phone_number': phone_number,
                        'company_name': company_name,
                        'is_trusted': True,
                        'is_seller': True,
                        'admin_created': True
                    }
                )

        self.stdout.write(self.style.SUCCESS('User profiles created successfully.'))

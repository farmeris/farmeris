
# edit_profile/management/commands/create_pobocky.py

import csv
import io
import uuid
from datetime import datetime

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand
from django.utils.text import slugify

from login_app.models import UserProfile
from edit_profile.models import Pobocka
from staticmap import StaticMap, CircleMarker

def generate_map_image(longitude, latitude):
    m = StaticMap(450, 450, url_template='http://a.tile.osm.org/{z}/{x}/{y}.png')
    outer_marker = CircleMarker((longitude, latitude), '#FFFFFF', 13)  # white border
    inner_marker = CircleMarker((longitude, latitude), 'red', 9)  # blue center
    m.add_marker(outer_marker)
    m.add_marker(inner_marker)
    image = m.render(zoom=14)
    image_bytes = io.BytesIO()
    image.save(image_bytes, format='PNG')
    image_bytes.seek(0)
    return ContentFile(image_bytes.getvalue())

class Command(BaseCommand):
    help = 'Create pobocky from a CSV file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='Path to the CSV file containing pobocky data')

    def handle(self, *args, **options):
        file_path = options['file_path']
        with open(file_path, 'r') as file:
            reader = csv.DictReader(file)
            for row in reader:
                username = row['username']
                try:
                    user = User.objects.get(username=username)
                    user_profile = UserProfile.objects.get(user=user)
                    longitude = row.get('longitude', '')
                    latitude = row.get('latitude', '')
                    if longitude and latitude:
                        try:
                            longitude = float(longitude)
                            latitude = float(latitude)

                            # Generate the map image content
                            map_image_content = generate_map_image(longitude, latitude)

                            # Create a unique filename for the map image
                            timestamp = datetime.now().strftime("%d-%m-%Y--%H-%M-%S")
                            unique_id = uuid.uuid4().hex[:8]
                            image_filename = f"pobocka_map_{username}_{timestamp}_{unique_id}.png"

                            pobocka = Pobocka.objects.create(
                                user_profile=user_profile,
                                longitude=longitude,
                                latitude=latitude,
                                location_pobocka=Point(longitude, latitude)
                            )

                            # Save the map image with the unique filename
                            pobocka.map_image.save(image_filename, map_image_content, save=True)

                        except ValueError:
                            self.stderr.write(self.style.WARNING(f"Invalid longitude or latitude value for user '{username}'. Skipping pobocka creation."))
                except User.DoesNotExist:
                    self.stderr.write(self.style.WARNING(f"User '{username}' does not exist. Skipping pobocka creation."))
                except UserProfile.DoesNotExist:
                    self.stderr.write(self.style.WARNING(f"UserProfile for user '{username}' does not exist. Skipping pobocka creation."))

        self.stdout.write(self.style.SUCCESS('Pobocky created successfully.'))

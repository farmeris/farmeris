
# login_app/constants.py

CURRENCY_CHOICES = [
    ('EUR', '€ Euro'),
    ('CZK', 'Kč Česká koruna'),
    ('HUF', 'Ft Magyar forint'),
    ('PLN', 'zł Polski złoty'),
    ('UAH', '₴ Українська гривня'),
    ('RON', 'lei Leu românesc'),
    ('USD', '$ US Dollar'),
    ('GBP', '£ British Pound')
]

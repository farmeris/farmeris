
#login_app/models.py

import uuid

import phonenumbers
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.geos import Point
from django.core.validators import RegexValidator, URLValidator
from django.db import models
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from login_app.constants import CURRENCY_CHOICES
from tagmanager.models import Tag
from login_app.validators import (validate_facebook_url,
                                  validate_instagram_url, validate_tiktok_url,
                                  validate_twitter_url, validate_youtube_url)
from phonenumbers import (NumberParseException, PhoneNumberFormat,
                          format_number, parse)


def upload_to2(instance, filename):
    truncated_username = slugify(instance.user.username[:10])

    # The rest of the logic remains the same:
    original_filename = os.path.splitext(filename)[0]
    slugified_filename = slugify(original_filename)
    unique_filename = f'{truncated_username}-{slugified_filename}-{uuid.uuid4()}'
    ext = filename.split('.')[-1]
    full_filename = f'{slugify(unique_filename)}.{ext}'

    # The storage path is modified. We'll store the avatar in a folder named 'avatar' inside the user's folder:
    return os.path.join('obrasteky', truncated_username, 'avatar', full_filename)

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    #color theme
    theme = models.CharField(max_length=10, choices=[('light', 'Light'), ('dark', 'Dark')], default='light')

    filter_trusted = models.BooleanField(_('filter trusted'), default=True)
    is_trusted = models.BooleanField(_('trusted user'), default=False)
    is_seller = models.BooleanField(_('seller user'), default=False, help_text=_('Indicates whether the user can post offers and transport info.'))
    admin_control_only = models.BooleanField(_('Admin is in control only. Needs mail confirmation from a user to be set to False'), default=False)

    preferred_currency = models.CharField(_('preferred currency'), max_length=3, choices=CURRENCY_CHOICES, default='EUR')

    # Socials
    avatar = models.ImageField(upload_to=upload_to2, null=True, blank=True)
    website = models.URLField(_('website'), max_length=255, blank=True, null=True)
    facebook = models.URLField(_('Facebook'), max_length=255, blank=True, null=True, validators=[URLValidator()])
    twitter = models.URLField(_('Twitter'), max_length=255, blank=True, null=True, validators=[URLValidator()])
    instagram = models.URLField(_('Instagram'), max_length=255, blank=True, null=True, validators=[URLValidator()])
    tiktok = models.URLField(_('TikTok'), max_length=255, blank=True, null=True, validators=[URLValidator()])
    youtube = models.URLField(_('YouTube'), max_length=255, blank=True, null=True, validators=[URLValidator()])

    # user details
    phone_country_code = models.CharField(_('phone country code'), max_length=5, default='+421', blank=True, null=True)
    phone_number = models.CharField(_('phone number'), max_length=20, blank=True, null=True, validators=[
        RegexValidator(
            r'^[\d\s/]+$',
            message=_("Enter a valid phone number. Only numbers, spaces, and '/' are allowed.")
        )]
    )
    full_mobile_number = models.CharField(max_length=30, blank=True, null=True)
    languages = models.CharField(_('languages'), max_length=100, blank=True, null=True)
    bio = models.TextField(_('bio'), blank=True, null=True)

    # google saved info
    google_name = models.CharField(max_length=255, blank=True, null=True)
    locale = models.CharField(max_length=10, blank=True, null=True)
    gender = models.CharField(max_length=20, blank=True, null=True)

    #saving pagination preference
    pagination_preference = models.PositiveIntegerField(default=10)
    sidebar_visible = models.BooleanField(default=False, verbose_name=_('Sidebar Visibility'))

    #languages
    PREFERRED_LANGUAGES = [
        ('sk', _('Slovak')),
        ('en', _('English')),
        ('cz', _('Czech')),
    ]
    preferred_language = models.CharField(_('preferred language'), max_length=10, choices=PREFERRED_LANGUAGES, default='sk')

    #company information
    company_name = models.CharField(_('company name'), max_length=255, blank=True, null=True)
    company_id_number = models.CharField(_('company identification number (IČO)'), max_length=10, blank=True, null=True, validators=[RegexValidator(r'^\d{8,10}$', message=_("Enter a valid IČO."))])
    tax_id_number = models.CharField(_('tax identification number (DIČ)'), max_length=12, blank=True, null=True)
    vat_number = models.CharField(_('VAT number (IČ DPH)'), max_length=14, blank=True, null=True, validators=[RegexValidator(r'^[a-zA-Z]{2}\d{8,10}$', message=_("Enter a valid VAT number."))])
    # sidlo spolocnosti
    company_address = models.CharField(_('company address'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = "Trust factor"

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        if self.phone_country_code and self.phone_number:
            # Concatenate country code and phone number
            full_number = f"{self.phone_country_code}{self.phone_number}".replace(" ", "")
            try:
                # Automatically determine the region from the country code
                region = phonenumbers.region_code_for_country_code(int(self.phone_country_code.replace('+', '')))
                parsed_number = parse(full_number, region)
                if phonenumbers.is_valid_number(parsed_number):
                    self.full_mobile_number = format_number(parsed_number, PhoneNumberFormat.INTERNATIONAL)
                else:
                    # If the number is not valid, consider logging or handling this case as needed
                    self.full_mobile_number = full_number  # Or leave it empty or set a default value
            except NumberParseException:
                # Handle parsing exceptions
                self.full_mobile_number = full_number  # Or leave it empty or set a default value

        super(UserProfile, self).save(*args, **kwargs)

class UserProfileTag(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='user_profile_tags')
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE, related_name='tagged_profiles')

    class Meta:
        unique_together = ('user_profile', 'tag')

    def __str__(self):
        return f"{self.user_profile.user.username} - {self.tag.name}"

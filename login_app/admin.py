from django.contrib import admin
from django.contrib.auth import get_user_model
from login_app.models import UserProfile

User = get_user_model()

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'is_trusted', 'user_is_active']
    actions = ['make_trusted', 'make_untrusted']

    def user_is_active(self, obj):
        return obj.user.is_active
    user_is_active.short_description = 'Is Active'
    user_is_active.boolean = True

    def make_trusted(self, request, queryset):
        queryset.update(is_trusted=True)
    make_trusted.short_description = "Mark selected users as trusted"

    def make_untrusted(self, request, queryset):
        queryset.update(is_trusted=False)
    make_untrusted.short_description = "Mark selected users as untrusted"

    # Rest of the class

admin.site.register(UserProfile, UserProfileAdmin)

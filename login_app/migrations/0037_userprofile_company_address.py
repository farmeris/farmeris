# Generated by Django 5.0.3 on 2024-04-06 17:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('login_app', '0036_remove_userprofile_city_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='company_address',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='company address'),
        ),
    ]

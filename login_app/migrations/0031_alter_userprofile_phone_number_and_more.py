# Generated by Django 5.0.3 on 2024-03-15 19:24

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('login_app', '0030_userprofile_company_address_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='phone_number',
            field=models.CharField(blank=True, max_length=20, null=True, validators=[django.core.validators.RegexValidator('^[\\d\\s/]+$', message="Enter a valid phone number. Only numbers, spaces, and '/' are allowed.")], verbose_name='phone number'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='zip_postal_code',
            field=models.CharField(blank=True, max_length=6, null=True, validators=[django.core.validators.RegexValidator('^\\d+(\\s\\d+)*$', message='Enter a valid postal code. Only numbers and spaces are allowed.')], verbose_name='zipcode'),
        ),
    ]

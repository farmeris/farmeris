#!/usr/bin/env python3

import re

from django import template

register = template.Library()

@register.filter(name='is_uuid_username')
def is_uuid_username(username):
    # Regular expression to match the UUID format followed by "-google"
    uuid_regex = re.compile(r'^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[4][0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}-google$')
    return bool(uuid_regex.match(username))


# login_app/templatetags/phone_filters.py

import phonenumbers
from django import template

register = template.Library()

@register.filter(name='format_phone')
def format_phone(value):
    try:
        phone_number = phonenumbers.parse(value, None)
        return phonenumbers.format_number(phone_number, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
    except phonenumbers.NumberParseException:
        return value

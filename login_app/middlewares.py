
# login_app/middlewares.py

from django.shortcuts import redirect
from django.http import JsonResponse
# from login_app.forms import ToggleSidebarForm
from django.utils.deprecation import MiddlewareMixin


def theme_switch_middleware(get_response):

    def middleware(request):
        if request.method == "POST" and "theme" in request.POST:
            theme_changed = False
            if request.user.is_authenticated:
                user_theme = request.user.userprofile.theme
                new_theme = 'dark' if user_theme == 'light' else 'light'
                request.user.userprofile.theme = new_theme
                request.user.userprofile.save()
                theme_changed = True
            else:
                theme = request.POST.get("theme", "light")
                request.session['theme'] = theme
                theme_changed = True
                
            # If it's an AJAX request, return JSON instead of a full page reload
            if theme_changed and request.headers.get('X-Requested-With') == 'XMLHttpRequest':
                return JsonResponse({'theme': request.user.userprofile.theme if request.user.is_authenticated else request.session.get('theme')})

        response = get_response(request)
        return response

    return middleware

def sidebar_toggle_middleware(get_response):
    def middleware(request):
        if request.user.is_authenticated:
            sidebar_visible = request.user.userprofile.sidebar_visible
        else:
            sidebar_visible = request.COOKIES.get('sidebar_visible', 'false').lower() in ['true', '1', 't']

        request.sidebar_visible = sidebar_visible

        if request.method == "POST" and "toggle_sidebar" in request.POST:
            if request.user.is_authenticated:
                profile = request.user.userprofile
                profile.sidebar_visible = not profile.sidebar_visible
                profile.save()
                request.sidebar_visible = profile.sidebar_visible
            else:
                new_visibility = not request.sidebar_visible
                request.sidebar_visible = new_visibility

        response = get_response(request)

        if request.method == "POST" and "toggle_sidebar" in request.POST:
            if not request.user.is_authenticated:
                # Set the updated sidebar visibility in the cookie
                response.set_cookie('sidebar_visible', request.sidebar_visible, max_age=31536000)  # Expires in 1 year
            if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
                # Respond to AJAX requests with the new state
                return JsonResponse({'sidebar_visible': request.sidebar_visible})

        return response

    return middleware

class NewUserRedirectMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if request.session.pop('new_oauth_user', False):
            return redirect('/accounts/set/username/')
        return response

#!/usr/bin/env python3

from django_hosts import host, patterns

host_patterns = patterns('',
    host(r'notes', 'notes.urls', name='notes'),
    host(r'www', 'farmeris.urls', name='www'),
)

#!/bin/bash

# Define the path to the Postfix configuration file
POSTFIX_MAIN_CF="/etc/postfix/main.cf"

# Backup the current configuration
cp $POSTFIX_MAIN_CF "${POSTFIX_MAIN_CF}.bak"

# Extract Docker container IPs and prepare them for Postfix
DOCKER_IPS=$(docker network inspect -f '{{range .IPAM.Config}}{{.Subnet}}{{end}}' $(docker network ls -q) | tr '\n' ' ')

# Define static networks
STATIC_NETWORKS="127.0.0.0/8 [::1]/128"

# Combine static networks with Docker IPs
MYNETWORKS="$STATIC_NETWORKS $DOCKER_IPS"

# Update Postfix configuration with new mynetworks
postconf -e "mynetworks = $MYNETWORKS"

# Reload Postfix to apply changes
postfix reload
